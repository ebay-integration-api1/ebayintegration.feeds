﻿using System;
using eBayMicroservices.Feeds.Application.Services;
using eBayMicroservices.Feeds.Infrastructure.Services;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.Extensions.DependencyInjection;

namespace eBayMicroservices.Feeds.Tests.Shared.Factories
{
    public class EBayMicroserviceAppFactory<TEntryPoint> : WebApplicationFactory<TEntryPoint> where TEntryPoint : class
    {
        protected override IWebHostBuilder CreateWebHostBuilder()
        {
            
            var p = Environment.GetEnvironmentVariable("RUNNER");
            Console.WriteLine($"RUNNER variable is : {p}");
            if(p == "linux") return base.CreateWebHostBuilder().UseEnvironment("tests_in_runner");
            return base.CreateWebHostBuilder().UseEnvironment("tests").ConfigureServices(x=> 
                    x.AddTransient<IEBayOrdersClient,EBayOrdersClientMock>()
                );
        }

    }
}