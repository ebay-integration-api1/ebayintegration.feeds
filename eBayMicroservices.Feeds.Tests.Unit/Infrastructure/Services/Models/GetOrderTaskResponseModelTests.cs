﻿using eBayMicroservices.Feeds.Infrastructure.Services.Models;
using Shouldly;
using Xunit;

namespace eBayMicroservices.Feeds.Tests.Unit.Infrastructure.Services.Models
{
    public class GetOrderTaskResponseModelTests
    {
        
        private static GetOrderTaskResponseModel Act(string feedType,string creationDate,string taskId,string status) => new GetOrderTaskResponseModel
        {
            Status = status,
            CreationDate = creationDate,
            FeedType = feedType,
            TaskId = taskId
        };

        [Fact]
        public void given_valid_data_should_be_succeed()
        {
            const string feedType = "feedType";
            const string creationDate = "creationDate";
            const string taskId = "taskId";
            const string status = "status";

            GetOrderTaskResponseModel result = Act(feedType, creationDate, taskId, status);
            
            result.ShouldNotBeNull();
            result.FeedType.ShouldBe(feedType);
            result.Status.ShouldBe(status);
            result.CreationDate.ShouldBe(creationDate);
            result.TaskId.ShouldBe(taskId);
        }
    }
}