﻿using System;
using eBayMicroservices.Feeds.Infrastructure.Services.Models;
using Shouldly;
using Xunit;

namespace eBayMicroservices.Feeds.Tests.Unit.Infrastructure.Services.Models
{
    public class CreateOrderRequestBodyModelTests
    {

        private static CreateOrderRequestBodyModel Act(DateTimeRange creation, DateTimeRange modified)
        => new CreateOrderRequestBodyModel
        {
            CreationDateRange = creation,
            ModifiedDateRange = modified
        };
        [Fact]
        public void given_dates_should_be_succeed()
        {
            DateTime start = DateTime.Now.AddHours(-1);
            DateTime end = DateTime.Now;
            DateTimeRange startRange = new DateTimeRange
            {
                From = start,
                To = end
            };
            DateTimeRange endRange = new DateTimeRange
            {
                From = start,
                To = end
            };

            CreateOrderRequestBodyModel result = Act(startRange, endRange);
            
            result.FeedType.ShouldBe("LMS_ORDER_REPORT");
            var rest = int.Parse(result.SchemaVersion) % 2;
            rest.ShouldBe(1);
            result.CreationDateRange.From.ShouldBe(start);
        }
    }
}