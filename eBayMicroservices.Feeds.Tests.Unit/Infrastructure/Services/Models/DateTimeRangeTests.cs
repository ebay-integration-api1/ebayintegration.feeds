﻿using System;
using eBayMicroservices.Feeds.Infrastructure.Services.Models;
using Shouldly;
using Xunit;

namespace eBayMicroservices.Feeds.Tests.Unit.Infrastructure.Services.Models
{
    public class DateTimeRangeTests
    {
        private static DateTimeRange Act(DateTime from,DateTime to) => new DateTimeRange
        {
            From = from,
            To = to
        };

        [Fact]
        public void given_valid_data_should_be_succeed()
        {
            DateTime from = DateTime.Now.AddHours(-1);
            DateTime to = DateTime.Now;

            DateTimeRange dateTimeRange = Act(from, to);
            
            dateTimeRange.ShouldNotBeNull();
            dateTimeRange.From.ShouldBe(from);
            dateTimeRange.To.ShouldBe(to);
        }
    }
}