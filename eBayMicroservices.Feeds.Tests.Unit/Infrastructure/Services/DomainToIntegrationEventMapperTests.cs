﻿using System;
using System.Linq;
using Convey.CQRS.Events;
using eBayMicroservices.Feeds.Core.Entities;
using eBayMicroservices.Feeds.Core.Events.Abstract;
using eBayMicroservices.Feeds.Infrastructure.Services;
using Shouldly;
using Xunit;

namespace eBayMicroservices.Feeds.Tests.Unit.Infrastructure.Services
{
    public class DomainToIntegrationEventMapperTests
    {
        private readonly DomainToIntegrationEventMapper _domainToIntegrationEventMapper;

        public DomainToIntegrationEventMapperTests()
        {
            _domainToIntegrationEventMapper = new DomainToIntegrationEventMapper();
        }

        private IEvent SingleAct(IDomainEvent domainEvent) => _domainToIntegrationEventMapper.Map(domainEvent);

        [Fact]
        public void give_undefined_domain_event_should_return_null()
        {
            Guid id = Guid.NewGuid();
            const string eBayRequestId = "eBayExample";
            Guid userId= Guid.NewGuid();
            const string marketPlace = "marketPlace";
            OperationType operationType = OperationType.GetOrders;
            
            OperationToCheck check = OperationToCheck.Create(id, eBayRequestId, userId, marketPlace, operationType);


            IDomainEvent @event = check.Events.SingleOrDefault();

            IEvent result = SingleAct(@event);
            
            result.ShouldBeNull();
        }
    }
}