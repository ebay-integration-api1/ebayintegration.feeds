﻿using System;
using System.Threading.Tasks;
using eBayMicroservices.Feeds.Application.Dto;
using eBayMicroservices.Feeds.Application.Exceptions.Concrete;
using eBayMicroservices.Feeds.Application.Queries;
using eBayMicroservices.Feeds.Application.Services;
using eBayMicroservices.Feeds.Infrastructure.Queries.Handlers;
using NSubstitute;
using Shouldly;
using Xunit;

namespace eBayMicroservices.Feeds.Tests.Unit.Infrastructure.Queries
{
    public class GetOrderByIdHandlerTests 
    {
        private readonly IEBayOrdersClient _ebayOrdersClient;
        private readonly IIdentityServiceClient _identityServiceClient;
        private readonly GetOrderByIdHandler _handler;

        private Task<OrderReportDto> Act(GetOrderByRequestId query) => _handler.HandleAsync(query);
        
        public GetOrderByIdHandlerTests()
        {
            _identityServiceClient = Substitute.For<IIdentityServiceClient>();
            _ebayOrdersClient = Substitute.For<IEBayOrdersClient>();
            _handler = new GetOrderByIdHandler(_ebayOrdersClient,_identityServiceClient);
        }

        [Fact]
        public async Task given_not_existing_user_should_throw_exception()
        {
            Guid userId = Guid.NewGuid();
            const string id = "example";
            
            // UserDto userDto = new UserDto
            // {
            //     Id = userId,
            //     Login = "userLogin",
            //     Name = "userName",
            //     AccessToken = "accessToken"
            // };
            //
            // _identityServiceClient.GetUserCredential(Arg.Is<Guid>(x => x == userId)).Returns(userDto);
            //
            GetOrderByRequestId query = new GetOrderByRequestId
            {
                Identifier = id,
                UserId = userId
            };

            Exception ex = await Record.ExceptionAsync(async () => await Act(query));
            
            ex.ShouldNotBeNull();
            ex.ShouldBeOfType<UserNotFoundException>();

            UserNotFoundException typedException = (UserNotFoundException) ex;
            
            typedException.Identifier.ShouldBe(userId.ToString());

        }
        
        
        [Fact]
        public async Task given_empty_identifier_should_throw_exception()
        {
            Guid userId = Guid.NewGuid();
            const string id = null;
            
            UserDto userDto = new UserDto
            {
                Id = userId,
                Login = "userLogin",
                Name = "userName",
                AccessToken = "accessToken"
            };

            _identityServiceClient.GetUserCredential(Arg.Is<Guid>(x => x == userId)).Returns(userDto);
            
            GetOrderByRequestId query = new GetOrderByRequestId
            {
                Identifier = id,
                UserId = userId
            };

            Exception ex = await Record.ExceptionAsync(async () => await Act(query));
            
            ex.ShouldNotBeNull();
            ex.ShouldBeOfType<InvalidIdentifierToGetOrderException>();

            InvalidIdentifierToGetOrderException typedException = (InvalidIdentifierToGetOrderException) ex;
            
            typedException.Identifier.ShouldBe(id);

        }
        
        [Fact]
        public async Task report_not_found_should_throw_exception()
        {
            Guid userId = Guid.NewGuid();
            const string id = "anyExistingId";
            
            UserDto userDto = new UserDto
            {
                Id = userId,
                Login = "userLogin",
                Name = "userName",
                AccessToken = "accessToken"
            };

            _identityServiceClient.GetUserCredential(Arg.Is<Guid>(x => x == userId)).Returns(userDto);
            
            GetOrderByRequestId query = new GetOrderByRequestId
            {
                Identifier = id,
                UserId = userId
            };

            
            Exception ex = await Record.ExceptionAsync(async () => await Act(query));
            
            ex.ShouldNotBeNull();
            ex.ShouldBeOfType<GetOrderReportResultException>();

            GetOrderReportResultException typedException = (GetOrderReportResultException) ex;
            
            typedException.Identifier.ShouldBe(id);

        }

        [Fact]
        public async Task report_found_should_be_succeed()
        {
            Guid userId = Guid.NewGuid();
            const string id = "anyExistingId";
            GetOrderByRequestId query = new GetOrderByRequestId
            {
                Identifier = id,
                UserId = userId
            };

            OrderReportDto orderReportDto = new OrderReportDto
            {
                RequestId = id
            };

            UserDto userDto = new UserDto
            {
                Id = Guid.NewGuid(),
                Login = "userLogin",
                Name = "userName",
                AccessToken = "accessToken"
            };

            _identityServiceClient.GetUserCredential(Arg.Is<Guid>(x => x == userId)).Returns(userDto);
            
            _ebayOrdersClient.GetOrderReport(userDto,id).Returns(orderReportDto);

            OrderReportDto result = await Act(query);
            result.ShouldBe(orderReportDto);

        }
    }
}