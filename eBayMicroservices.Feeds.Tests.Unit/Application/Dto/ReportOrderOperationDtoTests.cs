﻿using System;
using eBayMicroservices.Feeds.Application.Dto;
using Shouldly;
using Xunit;

namespace eBayMicroservices.Feeds.Tests.Unit.Application.Dto
{
    public class ReportOrderOperationDtoTests
    {

        private static ReportOrderOperationDto Act(Guid id, Guid userId, DateTime from, DateTime to)
            => new ReportOrderOperationDto
            {
                FromDate = from,
                OperationId = id,
                ToDate = to,
                UserId = userId
            };

        [Fact]
        public void given_valid_data_should_be_succeed_and_matching()
        {
            Guid id = Guid.NewGuid();
            Guid userId = Guid.NewGuid();
            DateTime from = DateTime.Now.AddDays(-1);
            DateTime to = DateTime.Now.AddDays(1);

            ReportOrderOperationDto result = Act(id, userId, from, to);
            
            result.ShouldNotBeNull();
            result.OperationId.ShouldBe(id);
            result.UserId.ShouldBe(userId);
            result.FromDate.ShouldBe(from);
            result.ToDate.ShouldBe(to);

        }
    }
}