﻿using System;
using eBayMicroservices.Feeds.Application.Dto;
using Shouldly;
using Xunit;

namespace eBayMicroservices.Feeds.Tests.Unit.Application.Dto
{
    public class UserDtoTests
    {

        public static UserDto Act(Guid id, string name, string login, string authToken) => new UserDto
        {
            Id = id,
            Login = login, 
            Name = name,
            AccessToken = authToken
        };

        [Fact]
        public void given_valid_data_should_be_succeed_and_matching()
        {
            Guid id = Guid.NewGuid();
            const string name = "name";
            const string login = "login";
            const string authToken = "authToken";

            UserDto result = Act(id, name, login, authToken);
            
            result.ShouldNotBeNull();
            result.Id.ShouldBe(id);
            result.Login.ShouldBe(login);
            result.Name.ShouldBe(name);
            result.AccessToken.ShouldBe(authToken);


        }
    }
}