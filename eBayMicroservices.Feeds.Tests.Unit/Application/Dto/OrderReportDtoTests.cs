﻿using eBayMicroservices.Feeds.Application.Dto;
using Shouldly;
using Xunit;

namespace eBayMicroservices.Feeds.Tests.Unit.Application.Dto
{
    public class OrderReportDtoTests
    {
        private static OrderReportDto Act(string requestId) => new OrderReportDto
        {
            RequestId = requestId
        };
        
        [Fact]
        public void given_request_id_should_be_succeed()
        {
            string requestId = "example";

            OrderReportDto result = Act(requestId);
            
            result.ShouldNotBeNull();
            result.RequestId.ShouldBe(requestId);
            
        }
    }
}