﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using eBayMicroservices.Feeds.Application.Events.Handlers;
using eBayMicroservices.Feeds.Application.Services;
using eBayMicroservices.Feeds.Core.Entities;
using eBayMicroservices.Feeds.Core.Events;
using eBayMicroservices.Feeds.Core.Events.Abstract;
using NSubstitute;
using Xunit;

namespace eBayMicroservices.Feeds.Tests.Unit.Application.Events.Handlers
{
    public class RequestOrderReportExecutedHandlerTests
    {
        private readonly IOperationsToCheckStorage _operationsToCheckStorage;
        private readonly IEventProcessor _eventProcessor;
        private readonly RequestOrderReportExecutedHandler _handler;

        public RequestOrderReportExecutedHandlerTests()
        {
            _operationsToCheckStorage = Substitute.For<IOperationsToCheckStorage>();
            _eventProcessor = Substitute.For<IEventProcessor>();
            _handler = new RequestOrderReportExecutedHandler(_operationsToCheckStorage, _eventProcessor);
        }

        private async Task Act(RequestOrderReportExecuted executed) => await _handler.HandleAsync(executed);
        
        [Fact]
        private async Task given_valid_data_should_be_succeed()
        {
            const string ebayRequest = "eBayRequest";
            DateTime creationDateTime = DateTime.Now;
            DateTime from = DateTime.Now.AddDays(-2);
            DateTime to = DateTime.Now.AddDays(-1);
            Guid userId = Guid.NewGuid();
            Guid id = Guid.NewGuid();
            const string marketPlace = "PL";
            const OperationType type = OperationType.GetOrders;

            OrderReportOperation operation = new OrderReportOperation(id,creationDateTime,userId,marketPlace,type,from,to);
            
            RequestOrderReportExecuted eve = new RequestOrderReportExecuted(operation,ebayRequest);

            await Act(eve);
            
            await _operationsToCheckStorage.Received().AddOperationToCheck(Arg.Is<OperationToCheck>(x =>
                x.Id.Value == id &&
                x.Type == type &&
                x.MarketPlace == marketPlace &&
                x.UserId == userId &&
                x.EBayRequestId == ebayRequest
                )
            );
            
            
            await _eventProcessor.Received().ProcessAsync(Arg.Is<IList<IDomainEvent>>(x =>
                (x[0] as OperationToCheckAdded).OperationToCheck.Id.Value == id &&
                (x[0] as OperationToCheckAdded).OperationToCheck.Type == type &&
                (x[0] as OperationToCheckAdded).OperationToCheck.MarketPlace == marketPlace&&
                (x[0] as OperationToCheckAdded).OperationToCheck.UserId == userId));
        }
    }
}