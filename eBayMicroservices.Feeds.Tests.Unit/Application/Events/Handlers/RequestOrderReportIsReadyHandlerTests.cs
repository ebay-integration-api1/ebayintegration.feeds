﻿using System;
using System.Threading.Tasks;
using eBayMicroservices.Feeds.Application.Events.Handlers;
using eBayMicroservices.Feeds.Core.Entities;
using eBayMicroservices.Feeds.Core.Events;
using Xunit;

namespace eBayMicroservices.Feeds.Tests.Unit.Application.Events.Handlers
{
    public class RequestOrderReportIsReadyHandlerTests
    {
        private readonly RequestOrderReportIsReadyHandler _handler;

        public RequestOrderReportIsReadyHandlerTests()
        {
            _handler = new RequestOrderReportIsReadyHandler();
        }

        private async Task Act(RequestOrderReportIsReady request) => await _handler.HandleAsync(request);

        [Fact]
        public async Task given_all_valid_data_should_be_succeed()
        {
            Guid id = Guid.NewGuid();
            Guid userId = Guid.NewGuid();
            const string eBayRequest = "eBayRequest";
            const string market = "market";
            const OperationType operationType = OperationType.GetOrders;
            
            OperationToCheck operationToCheck = OperationToCheck.Create(id,eBayRequest,userId,market,operationType);
            
            await Act(new RequestOrderReportIsReady(operationToCheck));
            
        }
    }
}