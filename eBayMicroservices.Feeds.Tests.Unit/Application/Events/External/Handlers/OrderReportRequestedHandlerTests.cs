﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using eBayMicroservices.Feeds.Application.Dto;
using eBayMicroservices.Feeds.Application.Events.External;
using eBayMicroservices.Feeds.Application.Events.External.Handlers;
using eBayMicroservices.Feeds.Application.Exceptions.Concrete;
using eBayMicroservices.Feeds.Application.Services;
using eBayMicroservices.Feeds.Core.Entities;
using eBayMicroservices.Feeds.Core.Events;
using eBayMicroservices.Feeds.Core.Events.Abstract;
using NSubstitute;
using Shouldly;
using Xunit;

namespace eBayMicroservices.Feeds.Tests.Unit.Application.Events.External.Handlers
{
    public class OrderReportRequestedHandlerTests
    {
        private readonly ITaskExecutorServiceClient _taskExecutorServiceClient;
        private readonly IOperationsToExecuteStorage _operationsToExecuteStorage;
        private readonly IEventProcessor _eventProcessor;
        private readonly OrderReportRequestedHandler _handler;
        public OrderReportRequestedHandlerTests()
        {
            _taskExecutorServiceClient = Substitute.For<ITaskExecutorServiceClient>();
            _operationsToExecuteStorage = Substitute.For<IOperationsToExecuteStorage>();
            _eventProcessor = Substitute.For<IEventProcessor>();
            _handler = new OrderReportRequestedHandler(_taskExecutorServiceClient,_operationsToExecuteStorage,_eventProcessor);
        }

        private async Task Act(OrderReportRequested req) => await _handler.HandleAsync(req);

        [Fact]
        public async Task given_not_existing_id_should_throw_exception()
        {
            Guid id = Guid.NewGuid();

            Exception ex = await Record.ExceptionAsync(async () => await Act(new OrderReportRequested {Id = id}));
            
            ex.ShouldNotBeNull();
            ex.ShouldBeOfType<OperationNotFound>();
        }
        
        
        [Fact]
        public async Task given_valid_existing_id_should_be_succeed()
        {
            Guid id = Guid.NewGuid();
            Guid userId = Guid.NewGuid();
            DateTime from = DateTime.Now.AddDays(-1);
            DateTime to = DateTime.Now;
            
            _taskExecutorServiceClient.GetReportOrderOperation(id).Returns(new ReportOrderOperationDto
            {
                OperationId = id,
                FromDate = from,
                ToDate = to,
                UserId = userId
            });
            
            await Act(new OrderReportRequested {Id = id});

            await _operationsToExecuteStorage.Received().AddOperationToExecute(Arg.Is<OrderReportOperation>(x => x.Id.Value == id));
            await _eventProcessor.Received().ProcessAsync(Arg.Is<IList<IDomainEvent>>(x => x[0] is OperationToExecuteAdded && (x[0] as OperationToExecuteAdded).OperationToExecute.Id.Value == id));

        }
    }
}