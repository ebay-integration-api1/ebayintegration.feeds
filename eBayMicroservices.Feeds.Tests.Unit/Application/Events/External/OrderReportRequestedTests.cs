﻿using System;
using eBayMicroservices.Feeds.Application.Events.External;
using Shouldly;
using Xunit;

namespace eBayMicroservices.Feeds.Tests.Unit.Application.Events.External
{
    public class OrderReportRequestedTests
    {
        public static OrderReportRequested Act(Guid id) => new OrderReportRequested
        {
            Id = id
        };

        [Fact]
        public void given_valid_id_should_be_succeed()
        {
            Guid id = Guid.NewGuid();
            OrderReportRequested result = Act(id);
            result.Id.ShouldBe(id);
        }
    }
}