﻿using System;
using eBayMicroservices.Feeds.Application.Events;
using Shouldly;
using Xunit;

namespace eBayMicroservices.Feeds.Tests.Unit.Application.Events
{
    public class OrderRequestReportPreparedTests
    {
        private static OrderRequestReportPrepared Act(Guid id, string eBayRequestId) =>
            new OrderRequestReportPrepared(id, eBayRequestId);

        [Fact]
        public void given_valid_data_should_be_succeed()
        {
            Guid id = Guid.NewGuid();
            const string eBayRequest = "eBayReq";

            OrderRequestReportPrepared result = Act(id, eBayRequest);
            
            result.ShouldNotBeNull();
            result.Id.ShouldBe(id);
            result.EBayRequestId.ShouldBe(eBayRequest);
        }
    }
}