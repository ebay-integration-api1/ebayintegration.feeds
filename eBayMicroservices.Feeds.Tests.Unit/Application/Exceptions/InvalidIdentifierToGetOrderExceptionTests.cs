﻿using eBayMicroservices.Feeds.Application.Exceptions.Abstract;
using eBayMicroservices.Feeds.Application.Exceptions.Concrete;
using Shouldly;
using Xunit;

namespace eBayMicroservices.Feeds.Tests.Unit.Application.Exceptions
{
    public class InvalidIdentifierToGetOrderExceptionTests
    {
        
        private static InvalidIdentifierToGetOrderException Act(string identifier) => new InvalidIdentifierToGetOrderException(identifier);


        [Fact]
        public void given_id_should_be_succeed()
        {
            const string identifier = "anyId";

            InvalidIdentifierToGetOrderException p = Act(identifier);
            
            p.ShouldNotBeNull();
            p.Identifier.ShouldBe(identifier);
        }
    }
}