﻿using eBayMicroservices.Feeds.Application.Exceptions.Concrete;
using Shouldly;
using Xunit;

namespace eBayMicroservices.Feeds.Tests.Unit.Application.Exceptions
{
    public class UnauthorisedUserExceptionTests
    {
        public static UnauthorisedUserException Act() => new UnauthorisedUserException();

        [Fact]
        public void create_exception_should_be_succeed()
        {
            UnauthorisedUserException result = Act();
            result.ShouldNotBeNull();
        }

    }
}