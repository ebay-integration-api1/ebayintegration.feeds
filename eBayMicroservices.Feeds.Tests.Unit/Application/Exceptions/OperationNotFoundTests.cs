﻿using System;
using eBayMicroservices.Feeds.Application.Exceptions.Concrete;
using Shouldly;
using Xunit;

namespace eBayMicroservices.Feeds.Tests.Unit.Application.Exceptions
{
    public class OperationNotFoundTests
    {
        
        private static OperationNotFound Act(Guid id) => new OperationNotFound(id.ToString());

        [Fact]
        public void given_id_should_be_succeed()
        {
            Guid id = Guid.NewGuid();
            OperationNotFound result = Act(id);
            
            result.ShouldNotBeNull();
            result.Id.ShouldBe(id.ToString());
        }

    }
}