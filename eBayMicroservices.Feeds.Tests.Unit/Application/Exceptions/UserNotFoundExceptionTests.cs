﻿using eBayMicroservices.Feeds.Application.Exceptions.Concrete;
using Shouldly;
using Xunit;

namespace eBayMicroservices.Feeds.Tests.Unit.Application.Exceptions
{
    public class UserNotFoundExceptionTests
    {
        
        private static UserNotFoundException Act(string identifier) => new UserNotFoundException(identifier);

        [Fact]
        public void given_id_should_be_succeed()
        {
            const string identifier = "login_identifier";
            UserNotFoundException result = Act(identifier);
            result.ShouldNotBeNull();
            result.Identifier.ShouldBe(identifier);
        }
    }
}