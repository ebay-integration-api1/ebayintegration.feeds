﻿using eBayMicroservices.Feeds.Application.Exceptions.Concrete;
using Shouldly;
using Xunit;

namespace eBayMicroservices.Feeds.Tests.Unit.Application.Exceptions
{
    public class UserAlreadyExistsTests
    {
        
        private static UserAlreadyExists Act(string login) => new UserAlreadyExists(login);

        [Fact]
        public void given_login_should_be_succeed()
        {
            const string login = "example_login";
            UserAlreadyExists result = Act(login);
            
            result.ShouldNotBeNull();
            result.Login.ShouldBe(login);
        }
    }
}