﻿using eBayMicroservices.Feeds.Application.Exceptions.Abstract;
using eBayMicroservices.Feeds.Application.Exceptions.Concrete;
using Shouldly;
using Xunit;

namespace eBayMicroservices.Feeds.Tests.Unit.Application.Exceptions
{
    public class GetOrderReportResultExceptionTests
    {
        private static GetOrderReportResultException Act(string identifier) => new GetOrderReportResultException(identifier);

        [Fact]
        public void given_valid_id_should_be_succeed()
        {
            const string identifier = "anyExistingIdentifier";

            GetOrderReportResultException result = Act(identifier);
            
            result.ShouldNotBeNull();
            
            result.Identifier.ShouldBe(identifier);
        }
    }
}