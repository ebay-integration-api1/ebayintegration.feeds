﻿namespace eBayMicroservices.Feeds.Tests.Unit.Application.Services.Responses
{
    public class EBayOrderReportRequestResponse
    {
        public bool Success { get; }
        public string RequestId { get; }

        public EBayOrderReportRequestResponse()
        {
            Success = false;
        }
        public EBayOrderReportRequestResponse( string requestId)
        {
            Success = true;
            RequestId = requestId;
        }
    }
}