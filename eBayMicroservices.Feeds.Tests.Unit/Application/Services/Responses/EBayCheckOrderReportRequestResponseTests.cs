﻿using eBayMicroservices.Feeds.Application.Services.Responses;
using Shouldly;
using Xunit;

namespace eBayMicroservices.Feeds.Tests.Unit.Application.Services.Responses
{
    public class EBayCheckOrderReportRequestResponseTests
    {
        private static EBayCheckOrderReportRequestResponse Act_NoParam() => new EBayCheckOrderReportRequestResponse();
        private static EBayCheckOrderReportRequestResponse Act_WithParam(bool isSuccess,string param) => new EBayCheckOrderReportRequestResponse(isSuccess,param);



        [Fact]
        private void given_no_parameter_should_be_succeed()
        {
            EBayCheckOrderReportRequestResponse result = Act_NoParam();
            
            result.IsReady.ShouldBe(false);
            result.RequestId.ShouldBeNull();
        }
        
        [Fact]
        private void given_parametersAndSuccess_should_be_succeed()
        {
            const string parameter = "example";
            const bool isSuccess = true;
            EBayCheckOrderReportRequestResponse result = Act_WithParam(isSuccess,parameter);
            
            result.IsReady.ShouldBe(true);
            result.IsSuccess.ShouldBe(isSuccess);
            result.RequestId.ShouldBe(parameter);
        }
        [Fact]
        private void given_parametersAndNotSuccess_should_be_succeed()
        {
            const string parameter = null;
            const bool isSuccess = false;
            EBayCheckOrderReportRequestResponse result = Act_WithParam(isSuccess,parameter);
            
            result.IsReady.ShouldBe(true);
            result.IsSuccess.ShouldBe(isSuccess);
            result.RequestId.ShouldBe(parameter);
        }
    }
}