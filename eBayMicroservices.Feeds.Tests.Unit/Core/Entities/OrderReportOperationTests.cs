﻿using System;
using eBayMicroservices.Feeds.Core.Entities;
using Shouldly;
using Xunit;

namespace eBayMicroservices.Feeds.Tests.Unit.Core.Entities
{
    public class OrderReportOperationTests
    {
        private static OrderReportOperation Act(Guid id, DateTime creationDateTime, Guid userId, 
            string marketPlace, OperationType type, DateTime from, DateTime to) 
            => OrderReportOperation.Create(id,creationDateTime,userId,marketPlace,type,from,to);

        [Fact]
        public static void valid_data_should_be_succeed()
        {
            Guid id = Guid.NewGuid();
            DateTime creationDateTime = DateTime.Now;
            Guid userId = Guid.NewGuid();
            string marketPlace = "EBAY_PL";
            OperationType type = OperationType.GetOrders;
            DateTime from = DateTime.Today.AddDays(-2);
            DateTime to = DateTime.Today.AddDays(-1);

            OrderReportOperation result = Act(id, creationDateTime, userId, marketPlace, type, from, to);
            
            result.From.ShouldBe(from);
            result.To.ShouldBe(to);
            result.Id.Value.ShouldBe(id);
            result.Type.ShouldBe(type);
            result.MarketPlace.ShouldBe(marketPlace);

        }
        
    }
}