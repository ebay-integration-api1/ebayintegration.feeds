﻿using System;
using System.Linq;
using eBayMicroservices.Feeds.Core.Entities;
using eBayMicroservices.Feeds.Core.Events;
using eBayMicroservices.Feeds.Core.Exceptions;
using Shouldly;
using Xunit;

namespace eBayMicroservices.Feeds.Tests.Unit.Core.Entities
{
    public class OperationToCheckTests
    {
        private static OperationToCheck Act(Guid id,string eBayRequestId,Guid userId,string marketPlace,OperationType operationType) =>
            OperationToCheck.Create(id,eBayRequestId ,userId,marketPlace,operationType);


        [Fact]
        public void given_invalid_eBayRequestId_should_throw_exception()
        {
            Guid id = Guid.NewGuid();
            string eBayRequestId = "";
            Guid userId= Guid.NewGuid();
            string marketPlace = "marketPlace";
            OperationType operationType = OperationType.GetOrders;

            Exception ex = Record.Exception(() => Act(id, eBayRequestId, userId, marketPlace, operationType));
            
            ex.ShouldNotBeNull();
            ex.ShouldBeOfType<InvalidRequestIdToCheckException>();

            InvalidRequestIdToCheckException typedEx = ex as InvalidRequestIdToCheckException;
            typedEx.ShouldNotBeNull();
            typedEx.OperationId.ShouldBe(id);
            typedEx.Type.ShouldBe(operationType);
            typedEx.GivenRequest.ShouldBe(eBayRequestId);
        }
        
        [Fact]
        public void given_valid_data_should_be_succeed()
        {
            Guid id = Guid.NewGuid();
            string eBayRequestId = "eBayExample";
            Guid userId= Guid.NewGuid();
            string marketPlace = "marketPlace";
            OperationType operationType = OperationType.GetOrders;

            OperationToCheck result = Act(id, eBayRequestId, userId, marketPlace, operationType);
            
            result.ShouldNotBeNull();
            result.Type.ShouldBe(operationType);
            result.MarketPlace.ShouldBe(marketPlace);
            result.UserId.ShouldBe(userId);
            result.EBayRequestId.ShouldBe(eBayRequestId);
            result.Id.Value.ShouldBe(id);
            result.CreationDateTime.ShouldNotBeNull();
            result.LastCompletedCheck.ShouldBeNull();
            result.CheckingStartDateTime.ShouldBeNull();

            result.Events.SingleOrDefault().ShouldBeOfType<OperationToCheckAdded>();
            
            (result.Events.SingleOrDefault() as OperationToCheckAdded)?.OperationToCheck.Id.Value.ShouldBe(id);

        }
        
    }
}