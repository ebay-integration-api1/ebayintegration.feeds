﻿using System;
using eBayMicroservices.Feeds.Core.Exceptions;
using Shouldly;
using Xunit;

namespace eBayMicroservices.Feeds.Tests.Unit.Core.Exceptions
{
    public class InvalidAggregateIdExceptionTests
    {
        public static InvalidAggregateIdException Act(Guid id) => new InvalidAggregateIdException(id);

        [Fact]
        public void given_id_should_be_succeed()
        {
            Guid id = Guid.NewGuid();

            InvalidAggregateIdException ex = Act(id);
            
            ex.ShouldNotBeNull();
            ex.Id.ShouldBe(id);

        }
    }
}