﻿using System;
using eBayMicroservices.Feeds.Core.Entities;
using eBayMicroservices.Feeds.Core.Exceptions;
using Shouldly;
using Xunit;

namespace eBayMicroservices.Feeds.Tests.Unit.Core.Exceptions
{
    public class InvalidRequestIdToCheckExceptionTests
    {
        private static InvalidRequestIdToCheckException Act(Guid operationId, string givenRequestId, OperationType type) 
            => new InvalidRequestIdToCheckException(operationId,givenRequestId,type);

        [Fact]
        public void given_data_should_be_succeed()
        {
            Guid operationId = Guid.NewGuid();
            const string givenRequestId = "requestId";
            const OperationType type = OperationType.GetOrders;

            InvalidRequestIdToCheckException result = Act(operationId, givenRequestId, type);
            
            result.ShouldNotBeNull();
            result.Type.ShouldBe(type);
            result.GivenRequest.ShouldBe(givenRequestId);
            result.OperationId.ShouldBe(operationId);
        }

        
    }
}