﻿using System;
using eBayMicroservices.Feeds.Core.Entities;
using eBayMicroservices.Feeds.Core.Events;
using eBayMicroservices.Feeds.Core.Events.Abstract;
using Shouldly;
using Xunit;

namespace eBayMicroservices.Feeds.Tests.Unit.Core.Events
{
    public class RequestOrderReportIsReadyTests : IDomainEvent
    {
        public static RequestOrderReportIsReady Act(OperationToCheck operationToCheck) => new RequestOrderReportIsReady(operationToCheck);
        
        
        [Fact]
        public void given_object_should_create_event()
        {
            Guid id = Guid.NewGuid();
            string eBayRequestId = "eBayExample";
            Guid userId= Guid.NewGuid();
            string marketPlace = "marketPlace";
            OperationType operationType = OperationType.GetOrders;
            
            OperationToCheck check = new OperationToCheck(id, eBayRequestId, userId, marketPlace, operationType);

            RequestOrderReportIsReady result = Act(check);
            
            result.ShouldNotBeNull();
            result.OperationToCheck.ShouldNotBeNull();
            result.OperationToCheck.Id.Value.ShouldBe(id);
            result.OperationToCheck.EBayRequestId.ShouldBe(eBayRequestId);
            result.OperationToCheck.UserId.ShouldBe(userId);
        }
    }
}