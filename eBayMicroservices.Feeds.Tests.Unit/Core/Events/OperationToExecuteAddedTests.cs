﻿using System;
using eBayMicroservices.Feeds.Core.Entities;
using eBayMicroservices.Feeds.Core.Events;
using Shouldly;
using Xunit;

namespace eBayMicroservices.Feeds.Tests.Unit.Core.Events
{
    public class OperationToExecuteAddedTests
    {
        private static OperationToExecuteAdded Act(OperationToExecute operationToCheck) => new OperationToExecuteAdded(operationToCheck);

        [Fact]
        public void given_valid_data_should_be_succeed()
        {
            Guid id = Guid.NewGuid();
            DateTime creationDateTime = DateTime.Now;
            Guid userId = Guid.NewGuid();
            string marketPlace = "EBAY_PL";
            OperationType type = OperationType.GetOrders;
            DateTime from = DateTime.Today.AddDays(-2);
            DateTime to = DateTime.Today.AddDays(-1);

            OperationToExecute operationToExecute = new OrderReportOperation(id, creationDateTime, userId, marketPlace, type, from, to);

            OperationToExecuteAdded result = Act(operationToExecute);
            
            result.ShouldNotBeNull();
            result.OperationToExecute.ShouldNotBeNull();
            result.OperationToExecute.Id.Value.ShouldBe(id);
            result.OperationToExecute.MarketPlace.ShouldBe(marketPlace);
            result.OperationToExecute.UserId.ShouldBe(userId);

        }
        
    }
}