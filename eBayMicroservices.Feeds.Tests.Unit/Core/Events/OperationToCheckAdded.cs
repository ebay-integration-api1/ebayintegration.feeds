﻿using System;
using eBayMicroservices.Feeds.Core.Entities;
using eBayMicroservices.Feeds.Core.Events;
using Shouldly;
using Xunit;

namespace eBayMicroservices.Feeds.Tests.Unit.Core.Events
{
    public class OperationToCheckAddedTests
    {
        private static OperationToCheckAdded Act(OperationToCheck operationToCheck) => new OperationToCheckAdded(operationToCheck);

        [Fact]
        public void given_object_should_create_event()
        {
            Guid id = Guid.NewGuid();
            const string eBayRequestId = "eBayExample";
            Guid userId= Guid.NewGuid();
            const string marketPlace = "marketPlace";
            const OperationType operationType = OperationType.GetOrders;
            
            OperationToCheck check = new OperationToCheck(id, eBayRequestId, userId, marketPlace, operationType);

            OperationToCheckAdded result = Act(check);
            
            result.ShouldNotBeNull();
            result.OperationToCheck.ShouldNotBeNull();
            result.OperationToCheck.Id.Value.ShouldBe(id);
            result.OperationToCheck.EBayRequestId.ShouldBe(eBayRequestId);
            result.OperationToCheck.UserId.ShouldBe(userId);
        }
    }
}