﻿using System;
using eBayMicroservices.Feeds.Core.Entities;
using eBayMicroservices.Feeds.Core.Events;
using Shouldly;
using Xunit;

namespace eBayMicroservices.Feeds.Tests.Unit.Core.Events
{
    public class RequestOrderReportExecutedTests
    {
        public static RequestOrderReportExecuted Act(OrderReportOperation operation, string eBayRequestId) =>
            new RequestOrderReportExecuted(operation, eBayRequestId);

        

        [Fact]
        public void given_valid_data_should_be_succeed()
        {
            Guid id = Guid.NewGuid();
            DateTime creationDateTime = DateTime.Now;
            Guid userId = Guid.NewGuid();
            string marketPlace = "EBAY_PL";
            string eBayRequest = "eBayRequest";
            OperationType type = OperationType.GetOrders;
            DateTime from = DateTime.Today.AddDays(-2);
            DateTime to = DateTime.Today.AddDays(-1);

            OrderReportOperation operationToExecute = new OrderReportOperation(id, creationDateTime, userId, marketPlace, type, from, to);

            RequestOrderReportExecuted result = Act(operationToExecute,eBayRequest);
            
            result.ShouldNotBeNull();
            result.Operation.ShouldNotBeNull();
            result.Operation.Id.Value.ShouldBe(id);
            result.Operation.MarketPlace.ShouldBe(marketPlace);
            result.Operation.UserId.ShouldBe(userId);
            result.EBayRequestId.ShouldBe(eBayRequest);

        }
        
    }
}