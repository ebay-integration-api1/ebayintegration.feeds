using System;
using System.IO;
using System.Reflection;
using System.Threading.Tasks;
using Convey;
using Convey.Logging;
using eBayMicroservices.Feeds.Api.AttributesDefinitions;
using eBayMicroservices.Feeds.Application;
using eBayMicroservices.Feeds.Infrastructure;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.OpenApi.Models;
using Open.Serialization.Json.Newtonsoft;

namespace eBayMicroservices.Feeds.Api
{
    public class Program
    {
        public static async Task Main(string[] args)
        {
            await CreateWebHostBuilder(args).Build().RunAsync();
        }

        public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
            WebHost.CreateDefaultBuilder(args).ConfigureServices(services =>
                    {
                        services.AddControllers();

                        services.TryAddSingleton(new JsonSerializerFactory().GetSerializer());
                        
                        services
                            .AddConvey()
                            .AddInfrastructure()
                            .AddApplication();

                        services.AddSwaggerGen(c =>
                        {
                            c.AddSecurityDefinition("Authorization",new OpenApiSecurityScheme
                            {
                                Description = "Set inside token returned from Sign-in method from /api/identity/sign-in",
                                In = ParameterLocation.Header,
                                Name = "JWT",
                                Type = SecuritySchemeType.ApiKey,
                                Scheme = "Authorization"
                                
                            });
                            c.SwaggerDoc("v1", new OpenApiInfo
                            {
                                Version = "v1",
                                Title = "eBay Identity Microservice",
                                Description = "Microservice which provides methods to authenticate " +
                                              "users with their credentials to eBay"
                            });
                            string xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                            string xmlFile2 = $"eBayMicroservices.Feeds.Application.xml";
                            string xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                            string xmlPath2 = Path.Combine(AppContext.BaseDirectory, xmlFile2);
                            c.OperationFilter<BasicAuthOperationsFilter>();
                            c.IncludeXmlComments(xmlPath);
                            c.IncludeXmlComments(xmlPath2);
                        });
                        
                        services.BuildServiceProvider();
                    }).Configure(app => app
                    .UseInfrastructure()
                    .UseRouting()
                    .UseSwagger(c =>
                    {
                        c.RouteTemplate = "api/feeds/swagger/{documentname}/swagger.json";
                    })
                    .UseSwaggerUI(c =>
                    {
                        c.SwaggerEndpoint("/api/feeds/swagger/v1/swagger.json", "My API V1");
                        c.RoutePrefix = "api/feeds/swagger";
                    })
                    .UseEndpoints(e => { e.MapControllers(); })
                    
                )
                .UseLogging();
    }
}