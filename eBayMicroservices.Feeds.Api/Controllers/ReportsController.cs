﻿using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Convey.CQRS.Queries;
using eBayMicroservices.Feeds.Application.Attributes;
using eBayMicroservices.Feeds.Application.Dto;
using eBayMicroservices.Feeds.Application.Queries;
using eBayMicroservices.Feeds.Application.Services;
using Microsoft.AspNetCore.Mvc;

namespace eBayMicroservices.Feeds.Api.Controllers
{
    [ApiController]
    [Route("api/reports")]
    public class ReportsController : ControllerBase
    {
        private readonly IQueryDispatcher _queryDispatcher;
        private readonly IIdentityProvider _identityProvider;

        public ReportsController(IQueryDispatcher queryDispatcher, IIdentityProvider identityProvider)
        {
            _queryDispatcher = queryDispatcher;
            _identityProvider = identityProvider;
        }

        [HttpGet("orders/{requestId}")]
        [OnlyLoggedInUser]
        [ProducesResponseType(typeof(OrderReportDto), 200)]
        [ProducesResponseType(typeof(object), 401)]
        public async Task<dynamic> GetOrder(string requestId)
        {
            OrderReportDto orderReportDto = await _queryDispatcher.QueryAsync(
                new GetOrderByRequestId
                {
                    Identifier = requestId,
                    UserId = _identityProvider.GetIdentity(HttpContext)
                });
            
            return File(orderReportDto.File,"application/octet-stream");
        }
    }
}