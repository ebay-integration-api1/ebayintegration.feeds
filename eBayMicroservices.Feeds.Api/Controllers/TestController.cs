﻿using System;
using System.Threading.Tasks;
using eBayMicroservices.Feeds.Application.Events.External;
using eBayMicroservices.Feeds.Application.Events.External.Handlers;
using eBayMicroservices.Feeds.Application.Services;
using Microsoft.AspNetCore.Mvc;

namespace eBayMicroservices.Feeds.Api.Controllers
{
    [ApiController]
    [Route("api")]
    public class TestController : ControllerBase
    {
        private readonly OrderReportRequestedHandler _orderReportRequestedHandler;

        public TestController(ITaskExecutorServiceClient taskExecutorServiceClient,IOperationsToExecuteStorage operationsToExecuteStorage, IEventProcessor eventProcessor)
        {
            _orderReportRequestedHandler = new OrderReportRequestedHandler(taskExecutorServiceClient,operationsToExecuteStorage,eventProcessor);
        }
#if DEBUG
        [ApiExplorerSettings(IgnoreApi = true)]
        [HttpGet("test")]
        public async Task Ping() => await _orderReportRequestedHandler.HandleAsync(new OrderReportRequested{Id = Guid.NewGuid()});
#endif
    }
}