﻿using Microsoft.AspNetCore.Mvc;

 namespace eBayMicroservices.Feeds.Api.Controllers
{
    [ApiController]
    [Route("api")]
    public class HomeController : ControllerBase
    {
        [ApiExplorerSettings(IgnoreApi = true)]
        [HttpGet("/ping")]
        public string Ping() => 
            "OK";
    }
}