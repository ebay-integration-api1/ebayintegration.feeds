﻿using System;
using Convey;
using Convey.CQRS.Queries;
using Convey.Discovery.Consul;
using Convey.HTTP;
using Convey.LoadBalancing.Fabio;
using Convey.MessageBrokers.CQRS;
using Convey.MessageBrokers.Outbox;
using Convey.MessageBrokers.RabbitMQ;
using Convey.Persistence.MongoDB;
using Convey.WebApi;
using Convey.WebApi.Exceptions;
using eBayMicroservices.Feeds.Application.Events.External;
using eBayMicroservices.Feeds.Application.Events.Handlers;
using eBayMicroservices.Feeds.Application.Services;
using eBayMicroservices.Feeds.Infrastructure.Exceptions;
using eBayMicroservices.Feeds.Infrastructure.Mongo.CollectionProviding.Abstract;
using eBayMicroservices.Feeds.Infrastructure.Mongo.CollectionProviding.Concrete;
using eBayMicroservices.Feeds.Infrastructure.Mongo.DateTimeConverter.Abstract;
using eBayMicroservices.Feeds.Infrastructure.Mongo.DateTimeConverter.Concrete;
using eBayMicroservices.Feeds.Infrastructure.Mongo.Documents.Abstract;
using eBayMicroservices.Feeds.Infrastructure.Mongo.Documents.Concrete;
using eBayMicroservices.Feeds.Infrastructure.Mongo.Repositories;
using eBayMicroservices.Feeds.Infrastructure.Quartz;
using eBayMicroservices.Feeds.Infrastructure.Quartz.Jobs.OperationExecuting;
using eBayMicroservices.Feeds.Infrastructure.Quartz.Jobs.OperationResultChecking;
using eBayMicroservices.Feeds.Infrastructure.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Server.Kestrel.Core;
using Microsoft.Extensions.DependencyInjection;
using Quartz;
using Quartz.Impl;
using Quartz.Spi;

namespace eBayMicroservices.Feeds.Infrastructure
{
    public static class Extensions
    {
        public static IConveyBuilder AddInfrastructure(this IConveyBuilder builder)
        {
            builder
                .AddQueryHandlers()
                .AddInMemoryQueryDispatcher()
                .AddHttpClient()
                .AddErrorHandler<ExceptionToResponseMapper>()
                .AddExceptionToMessageMapper<ExceptionToMessageMapper>()
                .AddRabbitMq()
                .AddMongo()
                .AddConsul()
                .AddFabio()
                .AddMessageOutbox()
                ;

            // ReSharper disable once RedundantTypeArgumentsOfMethod
            // ReSharper disable once RedundantCast
            builder.Services.Configure<KestrelServerOptions>((Action<KestrelServerOptions>) (o => o.AllowSynchronousIO = true));
            // ReSharper disable once RedundantTypeArgumentsOfMethod
            // ReSharper disable once RedundantCast
            builder.Services.Configure<IISServerOptions>((Action<IISServerOptions>) (o => o.AllowSynchronousIO = true));

            IScheduler scheduler = StdSchedulerFactory.GetDefaultScheduler().GetAwaiter().GetResult();

            scheduler.Start();
            
            IServiceCollection services = builder.Services;
            
            services.AddSingleton(scheduler);
            
            services.AddTransient<IDomainToIntegrationEventMapper,DomainToIntegrationEventMapper>();
            services.AddTransient<IEventProcessor,EventProcessor>();
            services.AddTransient<IMessageBroker,MessageBroker>();
            services.AddTransient<IObjectDocumentMapper,ObjectDocumentMapper>();
            services.AddTransient<IMongoCollectionProvider,MongoCollectionProvider>();
            services.AddTransient<IBaseEncodeConverter,BaseEncodeConverter>();
            services.AddTransient<IOperationsToExecuteStorage,OperationsToExecuteRepository>();
            services.AddTransient<IOperationsToCheckStorage,OperationsToCheckRepository>();
            services.AddTransient<IDateTimeConverter,DateTimeConverter>();
            services.AddTransient<IIdentityProvider,IdentityProvider>();
            services.AddTransient<IAuthenticator,Authenticator>();
            
            
            
            if (IsGitlabRunner() || IsNotK8SConfig())
            {
                services.AddTransient<IEBayOrdersClient,EBayOrdersClient>();
                services.AddTransient<IEBayServiceClient,EBayServiceClient>();
                services.AddTransient<ITaskExecutorServiceClient,TaskExecutorServiceClient>();
                services.AddTransient<IIdentityServiceClient,IdentityServiceClient>();
            }
            else
            {
                services.AddTransient<IEBayOrdersClient,EBayOrdersClient>();
                services.AddTransient<IEBayServiceClient,EBayServiceClient>();
                services.AddTransient<ITaskExecutorServiceClient,TaskExecutorServiceClient>();
                services.AddTransient<IIdentityServiceClient,IdentityServiceClient>();
            }

            services.AddSingleton<IExceptionToResponseMapper, ExceptionToResponseMapper>();
            //services.AddSwaggerGen();
            
            
            

            services.AddSingleton<IJobFactory,QuartzJobFactory>();
            services.AddSingleton<ISchedulerFactory,StdSchedulerFactory>();
            services.AddSingleton<SubmissionResultChecker>();
            services.AddSingleton<OperationExecutorJob>();
            services.AddSingleton(new SubmissionResultCheckerMetadata(Guid.NewGuid(), typeof(SubmissionResultChecker),"eBay Operation Result Job", "*/15 * * * * ?"));
            services.AddSingleton(new OperationExecutorJobMetadata(Guid.NewGuid(), typeof(OperationExecutorJob),"eBay Executor Job", "*/10 * * * * ?"));
            services.AddHostedService<QuartzHostedService>();
            


            builder.Services.Scan(s => s.FromAssemblies(AppDomain.CurrentDomain.GetAssemblies())
                .AddClasses(c => c.AssignableTo(typeof(IDomainEventHandler<>)))
                .AsImplementedInterfaces().WithTransientLifetime());
            
            return builder;
        }

        private static bool IsNotK8SConfig()
        {
            return Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT")?.ToLower() != "kubernetes";
            
        }

        private static bool IsGitlabRunner()
        {
            return Environment.GetEnvironmentVariable("RUNNER") == "linux";
        }


        public static IApplicationBuilder UseInfrastructure(this IApplicationBuilder app)
        {
            app
                .UseErrorHandler()
                .UseConvey()
                //.UsePublicContracts<ContractAttribute>()
                .UseRabbitMq()
                .SubscribeEvent<OrderReportRequested>();

            return app;
        }
    }
}