﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using eBayMicroservices.Feeds.Infrastructure.Quartz.Jobs.OperationExecuting;
using eBayMicroservices.Feeds.Infrastructure.Quartz.Jobs.OperationResultChecking;
using Microsoft.Extensions.Hosting;
using Quartz;
using Quartz.Spi;

namespace eBayMicroservices.Feeds.Infrastructure.Quartz
{
    public class QuartzHostedService : IHostedService
    {
        private readonly IJobFactory _jobFactory;
        private readonly ISchedulerFactory _schedulerFactory;
        private readonly SubmissionResultCheckerMetadata _submissionResultCheckerMetadata;
        private readonly OperationExecutorJobMetadata _operationExecutorJobMetadata;

        public QuartzHostedService(IScheduler scheduler, ISchedulerFactory schedulerFactory, IJobFactory jobFactory,
            SubmissionResultCheckerMetadata submissionResultCheckerMetadata,
            OperationExecutorJobMetadata operationExecutorJobMetadata)
        {
            Scheduler = scheduler;
            _schedulerFactory = schedulerFactory;
            _jobFactory = jobFactory;
            _submissionResultCheckerMetadata = submissionResultCheckerMetadata;
            _operationExecutorJobMetadata = operationExecutorJobMetadata;
        }

        public IScheduler Scheduler { get; set; }

        public async Task StartAsync(CancellationToken cancellationToken)
        {
            Scheduler = await _schedulerFactory.GetScheduler(cancellationToken);
            Scheduler.JobFactory = _jobFactory;

            IList<Tuple<IJobDetail, ITrigger>> list = new List<Tuple<IJobDetail, ITrigger>>();
            list.Add(new Tuple<IJobDetail, ITrigger>(
                CreateCheckResultJob(_submissionResultCheckerMetadata),
                CreateTrigger(_submissionResultCheckerMetadata)
            ));
            list.Add(new Tuple<IJobDetail, ITrigger>(
                CreateExecuteOperationJob(_operationExecutorJobMetadata),
                CreateTrigger(_operationExecutorJobMetadata)
            ));

            foreach (Tuple<IJobDetail, ITrigger> x in list)
            {
                await Scheduler.ScheduleJob(x.Item1, x.Item2, cancellationToken);
            }

            // ReSharper disable once PossibleNullReferenceException
            await Scheduler?.Start(cancellationToken);
            
        }

        public async Task StopAsync(CancellationToken cancellationToken)
        {
            // ReSharper disable once PossibleNullReferenceException
            await Scheduler?.Shutdown(true, cancellationToken);
        }

        private ITrigger CreateTrigger(SubmissionResultCheckerMetadata submissionResultCheckerMetadata)
        {
            return TriggerBuilder.Create()
                .WithIdentity(submissionResultCheckerMetadata.JobId.ToString())
                .WithCronSchedule(submissionResultCheckerMetadata.CronExpression)
                .WithDescription($"{submissionResultCheckerMetadata.JobName}")
            .Build();
        }

        private ITrigger CreateTrigger(OperationExecutorJobMetadata operationExecutorJobMetadata)
        {
            return TriggerBuilder.Create()
                .WithIdentity(operationExecutorJobMetadata.JobId.ToString())
                .WithCronSchedule(operationExecutorJobMetadata.CronExpression)
                .WithDescription($"{operationExecutorJobMetadata.JobName}")
                .Build();
        }

        private IJobDetail CreateCheckResultJob(SubmissionResultCheckerMetadata submissionResultCheckerMetadata)
        {
            return JobBuilder
                .Create(submissionResultCheckerMetadata.JobType)
                .WithIdentity(submissionResultCheckerMetadata.JobId.ToString())
                .WithDescription($"{submissionResultCheckerMetadata.JobName}")
                .Build();
        }

        private IJobDetail CreateExecuteOperationJob(OperationExecutorJobMetadata operationExecutorJobMetadata)
        {
            return JobBuilder
                .Create(operationExecutorJobMetadata.JobType)
                .WithIdentity(operationExecutorJobMetadata.JobId.ToString())
                .WithDescription($"{operationExecutorJobMetadata.JobName}")
                .Build();
        }
    }
}