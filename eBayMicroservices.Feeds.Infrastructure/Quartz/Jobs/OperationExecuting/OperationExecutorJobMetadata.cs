﻿using System;

namespace eBayMicroservices.Feeds.Infrastructure.Quartz.Jobs.OperationExecuting
{
    public class OperationExecutorJobMetadata
    {
        public Guid JobId { get;  }
        public Type JobType { get; }
        public string JobName { get; }
        public string CronExpression { get; }

        public OperationExecutorJobMetadata(Guid id, Type jobType, string jobName,
            string cronExpression)
        {
            JobId = id;
            JobType = jobType;
            JobName = jobName;
            CronExpression = cronExpression;
        }
    }
}