﻿using System;
using System.Threading.Tasks;
using eBayMicroservices.Feeds.Application.Dto;
using eBayMicroservices.Feeds.Application.Exceptions.Concrete;
using eBayMicroservices.Feeds.Application.Services;
using eBayMicroservices.Feeds.Application.Services.Responses;
using eBayMicroservices.Feeds.Core.Entities;
using Microsoft.Extensions.Logging;
using Quartz;

namespace eBayMicroservices.Feeds.Infrastructure.Quartz.Jobs.OperationExecuting
{
    //[DisallowConcurrentExecution]
    public class OperationExecutorJob : IJob
    {
        private readonly IOperationsToExecuteStorage _operationsStorage;
        private readonly IEBayServiceClient _eBayServiceClient;
        private readonly IIdentityServiceClient _identityServiceClient;
        private readonly ILogger<OperationExecutorJob> _logger;
        private static volatile bool _working;
        private readonly IEventProcessor _eventProcessor;

        public OperationExecutorJob(IEBayServiceClient eBayServiceClient, IOperationsToExecuteStorage operationsStorage,
            IIdentityServiceClient identityServiceClient,
            ILogger<OperationExecutorJob> logger,
            IEventProcessor eventProcessor)
        {
            _eBayServiceClient = eBayServiceClient;
            _operationsStorage = operationsStorage;
            _identityServiceClient = identityServiceClient;
            _logger = logger;
            _eventProcessor = eventProcessor;
        }

        public async Task Execute(IJobExecutionContext context)
        {
            if (_working)
            {
                return;
            }

            try
            {
                while (true)
                {
                    _working = true;
                    _logger.Log(LogLevel.Information, "Starting executor  ");

                    OperationToExecute operationToExecute =
                        await _operationsStorage.GetAndReserveOperationToExecute();

                    if (operationToExecute is null)
                    {
                        _logger.LogInformation("No operation to execute found");
                        return;
                    }

                    await ProcessOperation(operationToExecute);
                }
            }
            catch (EBayApiException ebayEx)
            {
                _logger.Log(LogLevel.Critical, ebayEx, ebayEx.Message);
            }
            catch (Exception ex)
            {
                _logger.Log(LogLevel.Critical, ex, ex.Message);
            }
            finally
            {
                _logger.Log(LogLevel.Information, "Stopping executor  ");
                _working = false;
            }
        }

        private async Task ProcessOperation(OperationToExecute operationToExecute)
        {
            UserDto userWithCredentialsCredentials =
                await _identityServiceClient.GetUserCredential(operationToExecute.UserId);

            if (userWithCredentialsCredentials is null)
            {
                throw new UserNotFoundException(operationToExecute.UserId.ToString());
            }
            
            if (operationToExecute.Type == OperationType.GetOrders)
            {
                if (!(operationToExecute is OrderReportOperation typedElem))
                {
                    _logger.LogError("typedElem after conversion is null");
                    return;
                }

                try
                {
                    EBayOrderReportRequestResponse response = await _eBayServiceClient.CreateOrderReport(
                        userWithCredentialsCredentials, operationToExecute.MarketPlace, typedElem.From,
                        typedElem.To, operationToExecute.Id.Value);

                    typedElem.MarkAsCreated(response.RequestId);
                }
                catch (EBayApiException ex)
                {
                    typedElem.MarkAsFailed(ex);
                }
                await _eventProcessor.ProcessAsync(typedElem.Events);
                await _operationsStorage.ReleaseOperationsToExecute(operationToExecute.UserId,
                    operationToExecute.MarketPlace, operationToExecute.Id.Value);
            }
        }
    }
}