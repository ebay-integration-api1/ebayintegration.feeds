﻿using System;
using System.Threading.Tasks;
using eBayMicroservices.Feeds.Application.Dto;
using eBayMicroservices.Feeds.Application.Services;
using eBayMicroservices.Feeds.Application.Services.Responses;
using eBayMicroservices.Feeds.Core.Entities;
using Microsoft.Extensions.Logging;
using Quartz;

namespace eBayMicroservices.Feeds.Infrastructure.Quartz.Jobs.OperationResultChecking
{
    [DisallowConcurrentExecution]
    public class SubmissionResultChecker : IJob
    {
        private readonly IOperationsToCheckStorage _operationsStorage;
        private readonly IEBayServiceClient _eBayServiceClient;
        private readonly IEventProcessor _eventProcessor;
        private readonly ILogger<SubmissionResultChecker> _logger;
        private readonly IIdentityServiceClient _identityServiceClient;
        private static bool _working;


        public SubmissionResultChecker(IOperationsToCheckStorage operationsStorage,
            IEBayServiceClient eBayServiceClient, IEventProcessor eventProcessor,
            ILogger<SubmissionResultChecker> logger,
            IIdentityServiceClient identityServiceClient)
        {
            _operationsStorage = operationsStorage;
            _eBayServiceClient = eBayServiceClient;
            _eventProcessor = eventProcessor;
            _logger = logger;
            _identityServiceClient = identityServiceClient;
        }

        public async Task Execute(IJobExecutionContext context)
        {
            if (_working)
            {
                return;
            }
            try
            {
                while (true)
                {
                    _working = true;
                    _logger.LogInformation("Starting checker  ");

                    OperationToCheck operationToChecks = await _operationsStorage.GetAndReserveOperationsToCheck();

                    if (operationToChecks is null)
                    {
                        _logger.LogInformation("No operation to check found");
                        return;
                    }

                    UserDto userWithCredentialsCredentials =
                        await _identityServiceClient.GetUserCredential(operationToChecks.UserId);

                    EBayCheckOrderReportRequestResponse eBayCheckOrderReportRequestResponse =
                        await _eBayServiceClient.CheckOrderReport(userWithCredentialsCredentials,
                            operationToChecks.EBayRequestId,operationToChecks.Id.Value);

                    if (!eBayCheckOrderReportRequestResponse.IsReady)
                    {
                        return;
                    }

                    await _operationsStorage.ReleaseOperationsToCheck(operationToChecks.UserId,
                        operationToChecks.MarketPlace,
                        operationToChecks.Id.Value);

                    operationToChecks.MarkAsCheckingFinish();

                    _logger.LogInformation(
                        $"Hello world from Checking job {operationToChecks.Type} ::::: {operationToChecks.Id.Value}");

                    await _eventProcessor.ProcessAsync(operationToChecks.Events);
                }
            }
            catch (Exception ex)
            {
                _logger.Log(LogLevel.Critical, ex, ex.Message);
            }
            finally
            {
                _logger.Log(LogLevel.Information, "Stopping checker  ");
                _working = false;
            }
        }
    }
}