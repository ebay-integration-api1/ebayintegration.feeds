﻿using System.Threading.Tasks;
using Convey.CQRS.Queries;
using eBayMicroservices.Feeds.Application.Dto;
using eBayMicroservices.Feeds.Application.Exceptions.Concrete;
using eBayMicroservices.Feeds.Application.Queries;
using eBayMicroservices.Feeds.Application.Services;

namespace eBayMicroservices.Feeds.Infrastructure.Queries.Handlers
{
    public class GetOrderByIdHandler : IQueryHandler<GetOrderByRequestId,OrderReportDto>
    {
        private readonly IEBayOrdersClient _ebayOrdersClient;
        private readonly IIdentityServiceClient _identityServiceClient;

        public GetOrderByIdHandler(IEBayOrdersClient ebayOrdersClient, IIdentityServiceClient identityServiceClient)
        {
            _ebayOrdersClient = ebayOrdersClient;
            _identityServiceClient = identityServiceClient;
        }

        public async Task<OrderReportDto> HandleAsync(GetOrderByRequestId query)
        {
            UserDto user = await _identityServiceClient.GetUserCredential(query.UserId);

            if (user is null)
            {
                throw new UserNotFoundException(query.UserId.ToString());
            }
            
            if (string.IsNullOrWhiteSpace(query.Identifier))
            {
                throw new InvalidIdentifierToGetOrderException(query.Identifier);
            }
            
            OrderReportDto result = await _ebayOrdersClient.GetOrderReport(user,query.Identifier);

            if (result is null)
            {
                throw new GetOrderReportResultException(query.Identifier);
            }

            return result;
        }
    }
}