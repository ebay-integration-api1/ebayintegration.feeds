﻿using System;
using System.Net;
using Convey.WebApi.Exceptions;
using eBayMicroservices.Feeds.Application.Exceptions.Abstract;
using eBayMicroservices.Feeds.Application.Exceptions.Concrete;
using eBayMicroservices.Feeds.Core.Exceptions.Abstract;

namespace eBayMicroservices.Feeds.Infrastructure.Exceptions
{
    // ReSharper disable once ClassNeverInstantiated.Global
    public class ExceptionToResponseMapper : IExceptionToResponseMapper
    {
        public ExceptionResponse Map(Exception exception)
            => exception switch
            {
                DomainException ex => new ExceptionResponse(new {code = ex.Code, reason = ex.Message},
                    HttpStatusCode.BadRequest),
                
                AppException ex => ex switch
                {
                    UnauthorisedUserException unauthorisedUserException
                        => new ExceptionResponse(
                            new
                            {
                                code = unauthorisedUserException.Code,
                                reason = unauthorisedUserException.Message
                            },
                            HttpStatusCode.Unauthorized),
                    UserNotFoundException userNotFoundException
                        => new ExceptionResponse(
                            new
                            {
                                code = userNotFoundException.Code,
                                reason = userNotFoundException.Message
                            },
                            HttpStatusCode.NotFound),
                    UserAlreadyExists userAlreadyExists
                        => new ExceptionResponse(
                            new
                            {
                                code = userAlreadyExists.Code,
                                reason = userAlreadyExists.Message
                            },
                            HttpStatusCode.Conflict),
                    InvalidIdentifierToGetOrderException invalid
                        => new ExceptionResponse(
                            new
                            {
                                code = invalid.Code,
                                reason = invalid.Message
                            },
                            HttpStatusCode.BadRequest),
                    GetOrderReportResultException invalid
                        => new ExceptionResponse(
                            new
                            {
                                code = invalid.Code,
                                reason = invalid.Message
                            },
                            HttpStatusCode.NotFound),
                    _ => new ExceptionResponse(
                        new
                        {
                            code = "error",
                            reason = "There was an error."
                        },
                        HttpStatusCode.BadRequest)
                },
                _ => new ExceptionResponse(new {code = "error", reason = "There was an error."},
                    HttpStatusCode.BadRequest)
            };
    }
}