﻿using System;
using Convey.MessageBrokers.RabbitMQ;
using eBayMicroservices.Feeds.Application.Events;
using eBayMicroservices.Feeds.Application.Exceptions.Concrete;

namespace eBayMicroservices.Feeds.Infrastructure.Exceptions
{
    // ReSharper disable once ClassNeverInstantiated.Global
    public class ExceptionToMessageMapper : IExceptionToMessageMapper
    {
        public object Map(Exception exception, object message)
            => exception switch
            {
                EBayApiException ex => new EBayApiExceptionEvent(ex.OperationId,ex.Context.ToString(),ex.StatusCode,ex.Message),
                _ => null
            };
    }
}