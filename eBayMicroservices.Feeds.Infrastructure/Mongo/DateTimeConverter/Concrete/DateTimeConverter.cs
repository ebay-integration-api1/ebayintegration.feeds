﻿using System;
using eBayMicroservices.Feeds.Infrastructure.Mongo.DateTimeConverter.Abstract;

namespace eBayMicroservices.Feeds.Infrastructure.Mongo.DateTimeConverter.Concrete
{
    internal sealed class DateTimeConverter : IDateTimeConverter
    {
        public long? Convert(DateTime? dt) => dt.HasValue ? Convert(dt.Value) : (long?)null;

        public long Convert(DateTime dt) => dt.Ticks;
        
        public DateTime? Convert(long? dt) => dt.HasValue? Convert(dt.Value): (DateTime?)null;
        
        public DateTime Convert(long dt) => new DateTime(dt);
    }
}