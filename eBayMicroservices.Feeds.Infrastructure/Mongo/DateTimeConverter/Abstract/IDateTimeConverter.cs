﻿using System;

namespace eBayMicroservices.Feeds.Infrastructure.Mongo.DateTimeConverter.Abstract
{
    public interface IDateTimeConverter
    {
        long? Convert(DateTime? dt);
        DateTime? Convert(long? dt);
        long Convert(DateTime dt);
        DateTime Convert(long dt);
    }
}