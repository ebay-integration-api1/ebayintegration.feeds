﻿using System;
using System.Threading.Tasks;
using eBayMicroservices.Feeds.Application.Services;
using eBayMicroservices.Feeds.Core.Entities;
using eBayMicroservices.Feeds.Infrastructure.Mongo.CollectionProviding.Abstract;
using eBayMicroservices.Feeds.Infrastructure.Mongo.Documents.Abstract;
using eBayMicroservices.Feeds.Infrastructure.Mongo.Documents.Objects;
using MongoDB.Driver;

namespace eBayMicroservices.Feeds.Infrastructure.Mongo.Repositories
{
    public class OperationsToCheckRepository : IOperationsToCheckStorage
    {
        private readonly IObjectDocumentMapper _objectToDocumentMapper;
        private readonly IMongoCollectionProvider _mongoCollectionProvider;

        public OperationsToCheckRepository(IObjectDocumentMapper objectToDocumentMapper,
            IMongoCollectionProvider mongoCollection)
        {
            _objectToDocumentMapper = objectToDocumentMapper;
            _mongoCollectionProvider = mongoCollection;
        }

        public async Task AddOperationToCheck(OperationToCheck operationToCheck)
        {
            IMongoCollection<OperationToCheckDocument> collection = _mongoCollectionProvider
                .OperationsToCheck(out _);
            
            await collection.InsertOneAsync(_objectToDocumentMapper.Convert(operationToCheck));
        }

        public async Task<OperationToCheck> GetAndReserveOperationsToCheck()
        {
            IMongoCollection<OperationToCheckDocument> mongoCollection = _mongoCollectionProvider
                .OperationsToCheck(out IMongoClient clint);

            using IClientSessionHandle session = await clint.StartSessionAsync();

            try
            {
                session.StartTransaction();
                DateTime curr = DateTime.UtcNow;
                
                SortDefinition<OperationToCheckDocument> sort =
                    Builders<OperationToCheckDocument>.Sort.Ascending(x => x.CreationDateTime);
                
                OperationToCheckDocument operationToCheckDocument = await mongoCollection
                    .Find(Builders<OperationToCheckDocument>.Filter.Or(
                        Builders<OperationToCheckDocument>.Filter.Lt(x=> x.CheckingDateTime,curr.AddMinutes(-1).Ticks),
                        Builders<OperationToCheckDocument>.Filter.Where(x=> !x.CheckingDateTime.HasValue))
                    )
                    .Sort(sort)
                    .FirstOrDefaultAsync();

                if (operationToCheckDocument is null)
                {
                    //_logger.LogInformation("No operations to check");

                    return null;
                }
                
                FilterDefinition<OperationToCheckDocument> filterDefinition =
                    Builders<OperationToCheckDocument>.Filter.Where(x => x.Id == operationToCheckDocument.Id);
                
                UpdateDefinition<OperationToCheckDocument> updateDefinition =
                    Builders<OperationToCheckDocument>.Update.Set(s => s.CheckingDateTime, curr.Ticks);


                await mongoCollection.UpdateOneAsync(filterDefinition, updateDefinition);

                await session.CommitTransactionAsync();

                return _objectToDocumentMapper.Convert(operationToCheckDocument);
            }
            catch (Exception)
            {
                await session.AbortTransactionAsync();
                throw;
            }
        }

        public async Task ReleaseOperationsToCheck(Guid userId,string marketPlace,Guid operationToCheck)
        {
            IMongoCollection<OperationToCheckDocument> mongoCollection = _mongoCollectionProvider
                .OperationsToCheck(out IMongoClient client);
            
            using IClientSessionHandle session = await client.StartSessionAsync();
            
            try
            {
                session.StartTransaction();
                
                await mongoCollection.DeleteOneAsync(x => x.Id == operationToCheck);

                await session.CommitTransactionAsync();
            }
            catch (Exception)
            {
                await session.AbortTransactionAsync();
                throw;
            }
        }
    }
}