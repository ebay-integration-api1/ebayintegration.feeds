﻿using System;
using System.Threading.Tasks;
using eBayMicroservices.Feeds.Application.Services;
using eBayMicroservices.Feeds.Core.Entities;
using eBayMicroservices.Feeds.Infrastructure.Mongo.CollectionProviding.Abstract;
using eBayMicroservices.Feeds.Infrastructure.Mongo.Documents.Abstract;
using eBayMicroservices.Feeds.Infrastructure.Mongo.Documents.Objects;
using Microsoft.Extensions.Logging;
using MongoDB.Driver;

namespace eBayMicroservices.Feeds.Infrastructure.Mongo.Repositories
{
    public class OperationsToExecuteRepository : IOperationsToExecuteStorage
    {
        private readonly IObjectDocumentMapper _objectToDocumentMapper;
        private readonly IMongoCollectionProvider _mongoCollectionProvider;
        private readonly ILogger<OperationsToExecuteRepository> _logger;

        public OperationsToExecuteRepository(IObjectDocumentMapper objectToDocumentMapper,
            IMongoCollectionProvider mongoCollectionProvider, ILogger<OperationsToExecuteRepository> logger)
        {
            _objectToDocumentMapper = objectToDocumentMapper;
            _mongoCollectionProvider = mongoCollectionProvider;
            _logger = logger;
        }

        public async Task AddOperationToExecute(OperationToExecute operation)
        {
            IMongoCollection<OperationToExecuteDocument> collection = _mongoCollectionProvider
                .OperationsToExecute(out IMongoClient _);
            
            await collection.InsertOneAsync(_objectToDocumentMapper.Convert(operation));
        }

        public async Task<OperationToExecute> GetAndReserveOperationToExecute()
        {
            IMongoCollection<OperationToExecuteDocument> mongoCollection = _mongoCollectionProvider
                .OperationsToExecute(out IMongoClient clint);

            using IClientSessionHandle session = await clint.StartSessionAsync();

            try
            {
                session.StartTransaction();
                DateTime curr = DateTime.UtcNow;

                SortDefinition<OperationToExecuteDocument> sort =
                    Builders<OperationToExecuteDocument>.Sort.Ascending(z => z.CreationDateTime);

                OperationToExecuteDocument bsonDocument =
                    (await mongoCollection.Find(
                        Builders<OperationToExecuteDocument>.Filter.Or(
                            Builders<OperationToExecuteDocument>.Filter.Lt(x => x.ExecutionDateTime,
                                curr.AddMinutes(-1).Ticks),
                            Builders<OperationToExecuteDocument>.Filter.Where(x => !x.ExecutionDateTime.HasValue))
                    ).Sort(sort).FirstOrDefaultAsync());

                if (bsonDocument is null)
                {
                    await session.AbortTransactionAsync();
                    return null;
                }

                FilterDefinition<OperationToExecuteDocument> filterDefinition =
                    Builders<OperationToExecuteDocument>.Filter.Where(xxx
                        => xxx.Id == bsonDocument.Id);


                UpdateDefinition<OperationToExecuteDocument> updateDefinition =
                    Builders<OperationToExecuteDocument>.Update
                        .Set(s => s.ExecutionDateTime, curr.Ticks);

                await mongoCollection.UpdateOneAsync(filterDefinition, updateDefinition);

                await session.CommitTransactionAsync();

                return _objectToDocumentMapper.Convert(bsonDocument);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex,ex.Message);
                await session.AbortTransactionAsync();
                throw;
            }
        }

        public async Task ReleaseOperationsToExecute(Guid userId, string marketPlace,Guid operations)
        {
            IMongoCollection<OperationToExecuteDocument> mongoCollection = _mongoCollectionProvider
                .OperationsToExecute(out IMongoClient client);

            using IClientSessionHandle session = await client.StartSessionAsync();

            try
            {
                session.StartTransaction();

                await mongoCollection.DeleteOneAsync(x => x.Id == operations && x.UserId == userId);
                
                await session.CommitTransactionAsync();
            }
            catch (Exception)
            {
                await session.AbortTransactionAsync();
                throw;
            }
        }
    }
}