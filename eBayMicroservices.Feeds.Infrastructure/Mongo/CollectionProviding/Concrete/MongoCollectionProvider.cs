﻿using System;
using System.Collections.Generic;
using eBayMicroservices.Feeds.Infrastructure.Mongo.CollectionProviding.Abstract;
using eBayMicroservices.Feeds.Infrastructure.Mongo.Documents.Objects;
using Microsoft.Extensions.Configuration;
using MongoDB.Driver;

namespace eBayMicroservices.Feeds.Infrastructure.Mongo.CollectionProviding.Concrete
{
    public class MongoCollectionProvider : IMongoCollectionProvider
    {
        private readonly IMongoClient _client;
        private readonly string _db;

        public MongoCollectionProvider(IConfiguration configuration)
        {
            string connString = configuration.GetSection("mongo").GetSection("connectionString").Value;
            _db = configuration.GetSection("mongo").GetSection("database").Value;
            MongoUrl mongoUrl = new MongoUrl(connString);
            MongoClientSettings mongoClientSettings = MongoClientSettings.FromUrl(mongoUrl);
            mongoClientSettings.RetryWrites = false;
            _client = new MongoClient(mongoClientSettings);
            
        }

        public IMongoCollection<T> UsersWithOperations<T>(out IMongoClient clint) where T : OperationCollectionDocumentBase
        {
            clint = _client;
            return _client.GetDatabase(_db).GetCollection<T>(CollectionNameMapping[typeof(T)]);
        }

        public IMongoCollection<OperationToExecuteDocument> OperationsToExecute(out IMongoClient clint)
        {
            clint = _client;
            return _client.GetDatabase(_db).GetCollection<OperationToExecuteDocument>("OperationsToExecute");
        }

        public IMongoCollection<OperationToCheckDocument> OperationsToCheck(out IMongoClient clint)
        {
            clint = _client;
            return _client.GetDatabase(_db).GetCollection<OperationToCheckDocument>("OperationsToCheck");
        }

        private static readonly IDictionary<Type, string> CollectionNameMapping = new Dictionary<Type, string>
        {
            {typeof(UserDocumentToCheck),"aaaOperationsToCheck"},
            {typeof(UserDocumentToExecute),"OaaaperationsToExecute"},
        };
        
    }
}