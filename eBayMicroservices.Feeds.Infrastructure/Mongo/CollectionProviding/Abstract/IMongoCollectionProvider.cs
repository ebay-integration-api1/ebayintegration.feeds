﻿using eBayMicroservices.Feeds.Infrastructure.Mongo.Documents.Objects;
using MongoDB.Driver;

namespace eBayMicroservices.Feeds.Infrastructure.Mongo.CollectionProviding.Abstract
{
    public interface IMongoCollectionProvider
    {
        //IMongoCollection<T> OperationDocumentCollection<T>(out IMongoClient client) where T : OperationDocumentBase;

        IMongoCollection<T> UsersWithOperations<T>(out IMongoClient clint) where T : OperationCollectionDocumentBase;

        IMongoCollection<OperationToExecuteDocument> OperationsToExecute(out IMongoClient clint);
        IMongoCollection<OperationToCheckDocument> OperationsToCheck(out IMongoClient clint);
        //IMongoCollection<UserDocumentToExecute> UsersWithOperationsToExecute(out IMongoClient clint);
    }
}