﻿using System;

namespace eBayMicroservices.Feeds.Infrastructure.Mongo.Projection
{
    public class IdWithSmallestDate
    {
        public Guid Id { get; set; }
        
    }
}