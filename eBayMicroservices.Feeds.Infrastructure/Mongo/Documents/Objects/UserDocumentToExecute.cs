﻿using System;
using System.Collections.Generic;

namespace eBayMicroservices.Feeds.Infrastructure.Mongo.Documents.Objects
{
    public class UserDocumentToExecute : OperationCollectionDocumentBase
    {
        public Guid Id { get; set; }
        public string MarketPlace { get; set;}
        public List<OperationToExecuteDocument> Operations { get; set; }
        public long? ExecutionDateTime { get; set; }
    }
}