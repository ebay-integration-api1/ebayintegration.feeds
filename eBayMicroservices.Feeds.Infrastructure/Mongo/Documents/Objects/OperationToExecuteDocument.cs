﻿using System;
using System.Collections.Generic;
using eBayMicroservices.Feeds.Core.Entities;

namespace eBayMicroservices.Feeds.Infrastructure.Mongo.Documents.Objects
{
    public class OperationToExecuteDocument
    {
        public Guid Id { get; set; }
        public Guid UserId { get; set; }
        public string MarketPlace { get; set; }
        public OperationType Type { get; set; }
        public long CreationDateTime { get; set; }
        public long? ExecutionDateTime { get; set; }
        public Guid? ParentOperationId { get; set; }
        public List<KeyValuePair<string,object>> Parameters { get; set; }
    }
}