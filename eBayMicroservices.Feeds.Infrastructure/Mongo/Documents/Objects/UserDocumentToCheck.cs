﻿using System;
using System.Collections.Generic;

namespace eBayMicroservices.Feeds.Infrastructure.Mongo.Documents.Objects
{
    public class UserDocumentToCheck : OperationCollectionDocumentBase
    {
        public Guid Id { get; set; }
        public string IdString { get; set; }
        public string MarketPlace { get; set; }
        public long LimitStart { get; set; }
        public int LimitValue { get; set; }
        public long LastUserOperationsExecute { get; set; }
        //variable which is changing after modify list with operations to avoid list enumerations
        public long SmallestOperationCreationDate { get; set; }
        public List<OperationToCheckDocument> OperationsToCheck { get; set; }
    }
}