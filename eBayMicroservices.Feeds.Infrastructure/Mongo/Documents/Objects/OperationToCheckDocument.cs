﻿using System;
using eBayMicroservices.Feeds.Core.Entities;

namespace eBayMicroservices.Feeds.Infrastructure.Mongo.Documents.Objects
{

    public class OperationToCheckDocument : OperationDocumentBase
    {
        public Guid Id { get; set; }
        public Guid UserId { get; set; }
        public string AmazonRequestId { get; set; }
        public string MarketPlace { get; set; }
        public long? LastCompletedCheckDateTime { get; set; }
        public long? CheckingDateTime { get; set; }
        public long CreationDateTime { get; set; }
        public OperationType Type { get; set; }
    }
}