﻿using eBayMicroservices.Feeds.Core.Entities;
using eBayMicroservices.Feeds.Infrastructure.Mongo.Documents.Objects;

namespace eBayMicroservices.Feeds.Infrastructure.Mongo.Documents.Abstract
{
    public interface IObjectDocumentMapper
    {
         OperationToCheck Convert (OperationToCheckDocument document);
         OperationToCheckDocument Convert(OperationToCheck operation);
        //
         OperationToExecute Convert(OperationToExecuteDocument document);
        // ShippingOverrideItem Convert(ShippingOverrideItemDocument document);
        // ShippingOverrideItemDocument Convert(ShippingOverrideItem item);
        //
         OperationToExecuteDocument Convert<T>(T operation) where T : OperationToExecute;
    }
}