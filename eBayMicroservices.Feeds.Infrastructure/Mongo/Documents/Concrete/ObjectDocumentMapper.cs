﻿using System;
using System.Collections.Generic;
using System.Linq;
using eBayMicroservices.Feeds.Core.Entities;
using eBayMicroservices.Feeds.Infrastructure.Mongo.DateTimeConverter.Abstract;
using eBayMicroservices.Feeds.Infrastructure.Mongo.Documents.Abstract;
using eBayMicroservices.Feeds.Infrastructure.Mongo.Documents.Objects;

namespace eBayMicroservices.Feeds.Infrastructure.Mongo.Documents.Concrete
{
    public class ObjectDocumentMapper : IObjectDocumentMapper
    {
        private readonly IDateTimeConverter _dateTimeConverter;

        public ObjectDocumentMapper(IDateTimeConverter dateTimeConverter)
        {
            _dateTimeConverter = dateTimeConverter;
        }

        public OperationToCheck Convert(OperationToCheckDocument document)

        {
            return document is null
                ? null
                : new OperationToCheck(document.Id, document.AmazonRequestId, document.UserId, document.MarketPlace,
                    document.Type, _dateTimeConverter.Convert(document.LastCompletedCheckDateTime),
                    _dateTimeConverter.Convert(document.CheckingDateTime));
        }

        public OperationToCheckDocument Convert(OperationToCheck operation)
        {
            return operation is null
                ? null
                : new OperationToCheckDocument
                {
                    Id = operation.Id.Value,
                    AmazonRequestId = operation.EBayRequestId,
                    UserId = operation.UserId,
                    MarketPlace = operation.MarketPlace,
                    CheckingDateTime = _dateTimeConverter.Convert(operation.CheckingStartDateTime),
                    LastCompletedCheckDateTime = _dateTimeConverter.Convert(operation.LastCompletedCheck),
                    CreationDateTime = _dateTimeConverter.Convert(operation.CreationDateTime),
                    Type = operation.Type
                };
        }

        public OperationToExecute Convert(OperationToExecuteDocument document)
            => document.Type switch
            {
                OperationType.GetOrders => ConvertOrderReportOperation(document),
                _ => null
            };

        public OrderReportOperation ConvertOrderReportOperation(OperationToExecuteDocument document)
        {
            List<KeyValuePair<string, object>> parameters = document.Parameters;
            long? from = (long?) parameters.FirstOrDefault(x => x.Key == "From").Value;
            long? to = (long?) parameters.FirstOrDefault(x => x.Key == "To").Value;

            return new OrderReportOperation(document.Id, _dateTimeConverter.Convert(document.CreationDateTime),
                document.UserId, document.MarketPlace, document.Type, new DateTime(from.Value), new DateTime(to.Value),
                document.ExecutionDateTime,document.ParentOperationId);
        }

        public OperationToExecuteDocument Convert<T>(T operation) where T : OperationToExecute
            => operation switch
            {
                OrderReportOperation op => new OperationToExecuteDocument
                {
                    Id = op.Id.Value,
                    Type = OperationType.GetOrders,
                    MarketPlace = op.MarketPlace,
                    UserId = op.UserId,
                    CreationDateTime = op.CreationDateTime.Ticks,
                    ParentOperationId = op.OperationParentId,
                    Parameters = new List<KeyValuePair<string, object>>
                    {
                        new KeyValuePair<string, object>("From", op.From.Ticks),
                        new KeyValuePair<string, object>("To", op.To.Ticks)
                    }
                },
                _ => null
            };
    }
}