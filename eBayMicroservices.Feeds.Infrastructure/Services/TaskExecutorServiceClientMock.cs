﻿using System;
using System.Threading.Tasks;
using eBayMicroservices.Feeds.Application.Dto;
using eBayMicroservices.Feeds.Application.Services;

namespace eBayMicroservices.Feeds.Infrastructure.Services
{
    public class TaskExecutorServiceClientMock : ITaskExecutorServiceClient
    {
#pragma warning disable 1998
        public async Task<ReportOrderOperationDto> GetReportOrderOperation(Guid id)
#pragma warning restore 1998
        {
            return new ReportOrderOperationDto
            {
                FromDate = DateTime.Today.AddDays(-1),
                OperationId = id,
                ToDate = DateTime.Today,
                UserId = Guid.NewGuid()
            };
        }
    }
}