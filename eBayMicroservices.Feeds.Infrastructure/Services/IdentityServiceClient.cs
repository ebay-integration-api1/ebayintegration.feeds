﻿using System;
using System.Threading.Tasks;
using Convey.HTTP;
using eBayMicroservices.Feeds.Application.Dto;
using eBayMicroservices.Feeds.Application.Services;

namespace eBayMicroservices.Feeds.Infrastructure.Services
{
    public class IdentityServiceClient : IIdentityServiceClient
    {
        private IHttpClient _httpClient;
        private string _url;

        public IdentityServiceClient(IHttpClient httpClient, HttpClientOptions httpClientOptions)
        {
            _httpClient = httpClient;
            _url = httpClientOptions.Services["identity"];
        }

        public Task<UserDto> GetUserCredential(Guid id) 
            => _httpClient.GetAsync<UserDto>($"{_url}/api/identity/{id}");
    }
}