﻿using System;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using eBayMicroservices.Feeds.Application.Dto;
using eBayMicroservices.Feeds.Application.Exceptions.Concrete;
using eBayMicroservices.Feeds.Application.Services;
using eBayMicroservices.Feeds.Application.Services.Responses;
using eBayMicroservices.Feeds.Infrastructure.Services.Models;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace eBayMicroservices.Feeds.Infrastructure.Services
{
    public class EBayServiceClient : IEBayServiceClient
    {
        private readonly IHttpClientFactory _httpClientFactory;
        private readonly ILogger<EBayServiceClient> _logger;

        public EBayServiceClient(IHttpClientFactory httpClientFactory, ILogger<EBayServiceClient> logger)
        {
            _httpClientFactory = httpClientFactory;
            _logger = logger;
        }

        public async Task<EBayOrderReportRequestResponse> CreateOrderReport(UserDto user,string marketplace, DateTime from, DateTime to,Guid operationId)
        {
            HttpResponseMessage httpResponseMessage;
            using (var httpClient = _httpClientFactory.CreateClient())
            {
                httpClient.DefaultRequestHeaders.Add("X-EBAY-C-MARKETPLACE-ID", marketplace);
                httpClient.DefaultRequestHeaders.Authorization =
                    new AuthenticationHeaderValue("Bearer", user.AccessToken);

                HttpContent content = PrepareRequestToMakeOrderReport(from, to);
                httpResponseMessage =
                    await httpClient.PostAsync(@"https://api.sandbox.ebay.com/sell/feed/v1/order_task", content);
            }

            if (!httpResponseMessage.IsSuccessStatusCode)
            {
                string responseContent = await httpResponseMessage.Content.ReadAsStringAsync();
                throw new EBayApiException(operationId,(int)httpResponseMessage.StatusCode,responseContent,EBayApiExceptionContext.RequestOrderReport);
            }

            string reportId = httpResponseMessage.Headers.Location.ToString().Split("/").Last();
            
            return new EBayOrderReportRequestResponse(reportId);
        }

        private static HttpContent PrepareRequestToMakeOrderReport(DateTime @from, DateTime to)
        {
            CreateOrderRequestBodyModel payloadObject = new CreateOrderRequestBodyModel
            {
                CreationDateRange = new DateTimeRange
                {
                    From = from,
                    To = to
                },
                ModifiedDateRange = new DateTimeRange
                {
                    From = from,
                    To = to
                }
            };
            string payload = JsonConvert.SerializeObject(payloadObject,
                new JsonSerializerSettings {ContractResolver = new CamelCasePropertyNamesContractResolver()});
            HttpContent content = new StringContent(payload, Encoding.UTF8, "application/json");
            return content;
        }

        public async Task<EBayCheckOrderReportRequestResponse> CheckOrderReport(UserDto user, string eBayRequestId,Guid operationId)
        {
            HttpResponseMessage httpResponseMessage;
            using (var httpClient = _httpClientFactory.CreateClient())
            {
                httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", user.AccessToken);
                httpResponseMessage =
                    await httpClient.GetAsync($"https://api.sandbox.ebay.com/sell/feed/v1/order_task/{eBayRequestId}");
            }
            
            string responseContent = await httpResponseMessage.Content.ReadAsStringAsync();
            
            if (!httpResponseMessage.IsSuccessStatusCode)
            {
                throw new EBayApiException(operationId,(int)httpResponseMessage.StatusCode,responseContent,EBayApiExceptionContext.GetOrderReportState);
            }

            GetOrderTaskResponseModel result = JsonConvert.DeserializeObject<GetOrderTaskResponseModel>(responseContent);
            _logger.Log(LogLevel.Information,$"StatusCode {httpResponseMessage.StatusCode}; Content {responseContent}");

            bool checkIsOperationCompleted = CheckIsOperationCompleted(result);
            if (checkIsOperationCompleted)
            {
                return new EBayCheckOrderReportRequestResponse(true,result.TaskId);
            }
            return result.Status.ToLower() == FailedStateKey 
                ? new EBayCheckOrderReportRequestResponse(false) 
                : new EBayCheckOrderReportRequestResponse();
        }

        private static bool CheckIsOperationCompleted(GetOrderTaskResponseModel result)
        {
            return result.Status.ToLower().Contains(CompletedStateKey);
        }

        private const string CompletedStateKey = "completed";
        private const string FailedStateKey = "failed";
    }
}