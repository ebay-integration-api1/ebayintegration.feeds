﻿using System;
using System.Threading.Tasks;
using eBayMicroservices.Feeds.Application.Dto;
using eBayMicroservices.Feeds.Application.Services;
using eBayMicroservices.Feeds.Application.Services.Responses;
#pragma warning disable 1998

namespace eBayMicroservices.Feeds.Infrastructure.Services
{
    public class EBayServiceClientMock : IEBayServiceClient
    {
        public async Task<EBayOrderReportRequestResponse> CreateOrderReport(UserDto user,string marketPlace, DateTime from, DateTime to,Guid operationId)
        {
            return new EBayOrderReportRequestResponse("mockedRequestId");
        }

        public async Task<EBayCheckOrderReportRequestResponse> CheckOrderReport(UserDto user, string eBayRequestId,Guid operationId)
        {
            return new EBayCheckOrderReportRequestResponse(true,"mockedRequestId");
        }
    }
}