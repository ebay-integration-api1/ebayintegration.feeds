﻿using System.IO;
using System.Threading.Tasks;
using eBayMicroservices.Feeds.Application.Dto;
using eBayMicroservices.Feeds.Application.Services;

namespace eBayMicroservices.Feeds.Infrastructure.Services
{
    public class EBayOrdersClientMock : IEBayOrdersClient
    {
#pragma warning disable 1998
        public async Task<OrderReportDto> GetOrderReport(UserDto user,string request)
#pragma warning restore 1998
        {
            if (request == "notFound")
            {
                return null;
            }
            
            return new OrderReportDto
            {
                RequestId = request,
                File = new MemoryStream()
            };
        }
    }
}