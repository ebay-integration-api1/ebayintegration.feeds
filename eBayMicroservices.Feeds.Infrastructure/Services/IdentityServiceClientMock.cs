﻿using System;
using System.Threading.Tasks;
using eBayMicroservices.Feeds.Application.Dto;
using eBayMicroservices.Feeds.Application.Services;

namespace eBayMicroservices.Feeds.Infrastructure.Services
{
    public class IdentityServiceClientMock : IIdentityServiceClient
    {
#pragma warning disable 1998
        public async Task<UserDto> GetUserCredential(Guid id)
#pragma warning restore 1998
        {
            return new UserDto
            {
                Id = id,
                Login = "MockedLogin",
                Name = "MockedName",
                AccessToken = "TokenMock"
            };
        }
    }
}