﻿using System.Collections.Generic;
using System.Linq;
using Convey.CQRS.Events;
using eBayMicroservices.Feeds.Application.Events;
using eBayMicroservices.Feeds.Application.Exceptions.Concrete;
using eBayMicroservices.Feeds.Application.Services;
using eBayMicroservices.Feeds.Core.Events;
using eBayMicroservices.Feeds.Core.Events.Abstract;

namespace eBayMicroservices.Feeds.Infrastructure.Services
{
    public class DomainToIntegrationEventMapper : IDomainToIntegrationEventMapper
    {
        public IEnumerable<IEvent> MapAll(IEnumerable<IDomainEvent> events) => events.Select(Map);

        public IEvent Map(IDomainEvent @event) => @event switch
        {
            RequestOrderReportIsReady re => new GenerateReportOrderCompleted
            {
                Id = re.OperationToCheck.Id.Value,
                EBayRequestId = re.OperationToCheck.EBayRequestId
            },
            RequestOrderReportFailed re
                => re.Exception switch
                {
                    EBayApiException eae => new EBayApiExceptionEvent(
                        re.OrderReportOperation.Id.Value,
                        eae.Context.ToString(),
                        re.Exception is EBayApiException eba ? eba.StatusCode : null,
                        eae.Message),
                    _ => new EBayApiExceptionEvent(
                        re.OrderReportOperation.Id.Value,
                        null,
                        null,
                        re.Exception.Message),
                },

            _ => null
        };
    }
}