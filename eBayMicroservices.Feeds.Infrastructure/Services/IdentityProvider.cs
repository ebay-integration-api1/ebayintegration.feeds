﻿using System;
using System.Linq;
using eBayMicroservices.Feeds.Application.Services;
using Microsoft.AspNetCore.Http;

namespace eBayMicroservices.Feeds.Infrastructure.Services
{
    public class IdentityProvider : IIdentityProvider
    {
        public Guid GetIdentity(HttpContext ctx) 
            => Guid.Parse(GetIdentityAsString(ctx));

        public string GetIdentityAsString(HttpContext ctx) 
            => ctx.Request.Headers["Authorization"].FirstOrDefault();
    }
}