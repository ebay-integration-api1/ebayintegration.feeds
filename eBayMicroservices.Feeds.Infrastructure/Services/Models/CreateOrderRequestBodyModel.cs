﻿namespace eBayMicroservices.Feeds.Infrastructure.Services.Models
{
    public class CreateOrderRequestBodyModel
    {
        public string FeedType => "LMS_ORDER_REPORT";

        public string SchemaVersion => "1113";
        public DateTimeRange CreationDateRange { get; set; }
        public DateTimeRange ModifiedDateRange { get; set; }
    }
}