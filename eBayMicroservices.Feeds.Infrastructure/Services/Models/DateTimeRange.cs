﻿using System;

namespace eBayMicroservices.Feeds.Infrastructure.Services.Models
{
    public class DateTimeRange
    {
        public DateTime From { get; set; }
        public DateTime To { get; set; }
    }
}