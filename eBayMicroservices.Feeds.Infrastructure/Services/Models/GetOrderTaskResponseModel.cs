﻿namespace eBayMicroservices.Feeds.Infrastructure.Services.Models
{
    public class GetOrderTaskResponseModel
    {
        public string FeedType { get; set; }
        public string CreationDate { get; set; }
        public string TaskId { get; set; }
        public string Status { get; set; }
    }
}