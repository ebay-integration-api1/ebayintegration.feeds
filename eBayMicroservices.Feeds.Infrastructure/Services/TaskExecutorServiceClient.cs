﻿using System;
using System.Threading.Tasks;
using Convey.HTTP;
using eBayMicroservices.Feeds.Application.Dto;
using eBayMicroservices.Feeds.Application.Services;

namespace eBayMicroservices.Feeds.Infrastructure.Services
{
    public class TaskExecutorServiceClient : ITaskExecutorServiceClient
    {

        private IHttpClient _httpClient;
        private string _url;

        public TaskExecutorServiceClient(IHttpClient httpClient, HttpClientOptions httpClientOptions)
        {
            _httpClient = httpClient;
            _url = httpClientOptions.Services["task-executor"];
        }

        public  Task<ReportOrderOperationDto> GetReportOrderOperation(Guid id)
            => _httpClient.GetAsync<ReportOrderOperationDto>($"{_url}/api/operation/requestOrderReport/{id}");
    }
}