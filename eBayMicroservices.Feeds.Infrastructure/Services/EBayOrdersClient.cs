﻿using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using eBayMicroservices.Feeds.Application.Dto;
using eBayMicroservices.Feeds.Application.Exceptions.Concrete;
using eBayMicroservices.Feeds.Application.Services;
using Microsoft.Extensions.Logging;

namespace eBayMicroservices.Feeds.Infrastructure.Services
{
    public class EBayOrdersClient : IEBayOrdersClient
    {
        private readonly IHttpClientFactory _httpClientFactory;
        private readonly ILogger<EBayOrdersClient> _logger;

        public EBayOrdersClient(IHttpClientFactory httpClientFactory, ILogger<EBayOrdersClient> logger)
        {
            _httpClientFactory = httpClientFactory;
            _logger = logger;
        }

        public async Task<OrderReportDto> GetOrderReport(UserDto user,string request)
        {
            HttpResponseMessage httpResponseMessage;
            using (HttpClient httpClient = _httpClientFactory.CreateClient())
            {
                httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", user.AccessToken);
                httpResponseMessage =
                    await httpClient.GetAsync($"https://api.sandbox.ebay.com/sell/feed/v1/task/{request}/download_result_file");
            }
            _logger.Log(LogLevel.Information,$"StatusCode {httpResponseMessage.StatusCode};");
            
            if (!httpResponseMessage.IsSuccessStatusCode)
            {
                throw new GetFileResultEbayException((int)httpResponseMessage.StatusCode);
            }

            Stream stream = await httpResponseMessage.Content.ReadAsStreamAsync();
            
            return new OrderReportDto
            {
                File = stream,
                RequestId = request
            };
            

            
        }
    }
}