﻿using System;
using Convey.CQRS.Queries;
using eBayMicroservices.Feeds.Application.Dto;

namespace eBayMicroservices.Feeds.Application.Queries
{
    public class GetOrderByRequestId : IQuery<OrderReportDto>
    {
        public string Identifier { get; set; }
        public Guid UserId { get; set; }
    }
}