﻿namespace eBayMicroservices.Feeds.Application.Services.Responses
{
    public class EBayOrderReportRequestResponse
    {
        public string RequestId { get; }

        public EBayOrderReportRequestResponse( string requestId)
        {
            RequestId = requestId;
        }
    }
}