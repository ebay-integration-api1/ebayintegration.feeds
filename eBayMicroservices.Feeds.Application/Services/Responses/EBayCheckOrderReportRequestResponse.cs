﻿namespace eBayMicroservices.Feeds.Application.Services.Responses
{
    public class EBayCheckOrderReportRequestResponse
    {
        public bool IsReady { get; }
        public bool IsSuccess { get; }
        public string RequestId { get; }

        public EBayCheckOrderReportRequestResponse()
        {
            IsReady = false;
            IsSuccess = false;
        }
        public EBayCheckOrderReportRequestResponse(bool isSuccess,string requestId=null)
        {
            IsSuccess = isSuccess;
            IsReady = true;
            RequestId = requestId;
        }
    }
}