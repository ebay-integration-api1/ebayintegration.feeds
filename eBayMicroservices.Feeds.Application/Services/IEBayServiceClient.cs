﻿using System;
using System.Threading.Tasks;
using eBayMicroservices.Feeds.Application.Dto;
using eBayMicroservices.Feeds.Application.Services.Responses;

namespace eBayMicroservices.Feeds.Application.Services
{
    public interface IEBayServiceClient
    {
        Task<EBayOrderReportRequestResponse> CreateOrderReport(UserDto user,string marketplace,DateTime from, DateTime to,Guid operationId);
        
        Task<EBayCheckOrderReportRequestResponse> CheckOrderReport(UserDto user,string eBayRequestId,Guid operationId);
    }
}