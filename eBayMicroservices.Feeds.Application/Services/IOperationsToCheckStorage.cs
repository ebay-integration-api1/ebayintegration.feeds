﻿using System;
using System.Threading.Tasks;
using eBayMicroservices.Feeds.Core.Entities;

namespace eBayMicroservices.Feeds.Application.Services
{
    public interface IOperationsToCheckStorage
    {
        Task AddOperationToCheck(OperationToCheck operationToCheck);
        Task<OperationToCheck> GetAndReserveOperationsToCheck();
        Task ReleaseOperationsToCheck(Guid userId,string marketPlace,Guid operationToCheck);
    }
}