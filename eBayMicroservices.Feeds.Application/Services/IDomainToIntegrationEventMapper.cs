﻿using System.Collections.Generic;
using Convey.CQRS.Events;
using eBayMicroservices.Feeds.Core.Events.Abstract;

namespace eBayMicroservices.Feeds.Application.Services
{
    public interface IDomainToIntegrationEventMapper
    {
        IEnumerable<IEvent> MapAll(IEnumerable<IDomainEvent> events);
        IEvent Map(IDomainEvent @event);
        
    }
}