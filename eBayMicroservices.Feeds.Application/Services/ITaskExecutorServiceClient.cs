﻿using System;
using System.Threading.Tasks;
using eBayMicroservices.Feeds.Application.Dto;

namespace eBayMicroservices.Feeds.Application.Services
{
    public interface ITaskExecutorServiceClient
    {
        Task<ReportOrderOperationDto> GetReportOrderOperation(Guid id);
    }
}