﻿using System.Threading.Tasks;
using eBayMicroservices.Feeds.Application.Dto;

namespace eBayMicroservices.Feeds.Application.Services
{
    public interface IEBayOrdersClient
    {
        Task<OrderReportDto> GetOrderReport(UserDto user,string request);
    }
}