﻿using Microsoft.AspNetCore.Http;

namespace eBayMicroservices.Feeds.Application.Services
{
    public interface IAuthenticator
    {
        public void Authenticate(HttpContext ctx);
    }
}