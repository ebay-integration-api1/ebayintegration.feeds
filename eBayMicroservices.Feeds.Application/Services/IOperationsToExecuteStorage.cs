﻿using System;
using System.Threading.Tasks;
using eBayMicroservices.Feeds.Core.Entities;

namespace eBayMicroservices.Feeds.Application.Services
{
    public interface IOperationsToExecuteStorage
    {
        Task AddOperationToExecute(OperationToExecute operation);
        Task<OperationToExecute> GetAndReserveOperationToExecute();
        Task ReleaseOperationsToExecute(Guid userId,string marketPlace,Guid operationId);
    }
}