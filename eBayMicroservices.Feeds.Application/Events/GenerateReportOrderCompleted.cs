﻿using System;
using Convey.CQRS.Events;

namespace eBayMicroservices.Feeds.Application.Events
{
    public class GenerateReportOrderCompleted : IEvent
    {
        public Guid Id { get; set; }
        public string EBayRequestId { get; set; }
    }
}