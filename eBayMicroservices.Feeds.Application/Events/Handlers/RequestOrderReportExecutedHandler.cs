﻿using System.Threading.Tasks;
using eBayMicroservices.Feeds.Application.Services;
using eBayMicroservices.Feeds.Core.Entities;
using eBayMicroservices.Feeds.Core.Events;

namespace eBayMicroservices.Feeds.Application.Events.Handlers
{
    public class RequestOrderReportExecutedHandler : IDomainEventHandler<RequestOrderReportExecuted>
    {
        private readonly IOperationsToCheckStorage _operationsToCheckStorage;
        private readonly IEventProcessor _eventProcessor;

        public RequestOrderReportExecutedHandler(IOperationsToCheckStorage operationsToCheckStorage,
            IEventProcessor eventProcessor)
        {
            _operationsToCheckStorage = operationsToCheckStorage;
            _eventProcessor = eventProcessor;
        }

        public async Task HandleAsync(RequestOrderReportExecuted @event)
        {
            OperationToCheck operationToCheck = OperationToCheck.Create(@event.Operation.Id.Value, @event.EBayRequestId,
                @event.Operation.UserId, @event.Operation.MarketPlace, OperationType.GetOrders);

            await _operationsToCheckStorage.AddOperationToCheck(operationToCheck);

            await _eventProcessor.ProcessAsync(operationToCheck.Events);
        }
    }
}