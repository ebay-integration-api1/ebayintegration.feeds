﻿using System;
using System.Threading.Tasks;
using eBayMicroservices.Feeds.Core.Events;

namespace eBayMicroservices.Feeds.Application.Events.Handlers
{
    public class RequestOrderReportIsReadyHandler : IDomainEventHandler<RequestOrderReportIsReady>
    {
        public Task HandleAsync(RequestOrderReportIsReady @event)
        {
            Console.WriteLine($"And Now send event that Operation (id:{@event.OperationToCheck.Id.Value}) is ready with eBayId:{@event.OperationToCheck.EBayRequestId}");
            return Task.CompletedTask;
        }
    }
}