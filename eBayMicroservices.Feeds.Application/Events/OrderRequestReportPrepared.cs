﻿using System;

namespace eBayMicroservices.Feeds.Application.Events
{
    public class OrderRequestReportPrepared
    {
        public OrderRequestReportPrepared(Guid id, string eBayRequestId)
        {
            Id = id;
            EBayRequestId = eBayRequestId;
        }
        public Guid Id { get; }
        public string EBayRequestId { get; }
    }
}