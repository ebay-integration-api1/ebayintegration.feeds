﻿using System;
using System.Threading.Tasks;
using Convey.CQRS.Events;
using eBayMicroservices.Feeds.Application.Dto;
using eBayMicroservices.Feeds.Application.Exceptions.Concrete;
using eBayMicroservices.Feeds.Application.Services;
using eBayMicroservices.Feeds.Core.Entities;

namespace eBayMicroservices.Feeds.Application.Events.External.Handlers
{
    public class OrderReportRequestedHandler : IEventHandler<OrderReportRequested>
    {
        private readonly ITaskExecutorServiceClient _taskExecutorServiceClient;
        private readonly IOperationsToExecuteStorage _operationsToExecuteStorage;
        private readonly IEventProcessor _eventProcessor;
        public OrderReportRequestedHandler(ITaskExecutorServiceClient taskExecutorServiceClient, IOperationsToExecuteStorage operationsToExecuteStorage, IEventProcessor eventProcessor)
        {
            _taskExecutorServiceClient = taskExecutorServiceClient;
            _operationsToExecuteStorage = operationsToExecuteStorage;
            _eventProcessor = eventProcessor;
        }

        public async Task HandleAsync(OrderReportRequested @event)
        {
            ReportOrderOperationDto reportOrderOperation =
                await _taskExecutorServiceClient.GetReportOrderOperation(@event.Id);

            if (reportOrderOperation is null)
            {
                throw new OperationNotFound(@event.Id.ToString());
            }
            
            OrderReportOperation orderReportOperation = OrderReportOperation.Create(reportOrderOperation.OperationId,
                DateTime.UtcNow, reportOrderOperation.UserId, reportOrderOperation.Marketplace, OperationType.GetOrders,
                reportOrderOperation.FromDate, reportOrderOperation.ToDate);

            await _operationsToExecuteStorage.AddOperationToExecute(orderReportOperation);

            await _eventProcessor.ProcessAsync(orderReportOperation.Events);

            ;
        }
    }
}