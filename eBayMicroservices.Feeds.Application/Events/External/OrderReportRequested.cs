﻿using System;
using Convey.CQRS.Events;
using Convey.MessageBrokers;

namespace eBayMicroservices.Feeds.Application.Events.External
{
    [Message("tasks","order_report_requested")]
    public class OrderReportRequested : IEvent
    {
        public Guid Id { get; set; }
    }
}