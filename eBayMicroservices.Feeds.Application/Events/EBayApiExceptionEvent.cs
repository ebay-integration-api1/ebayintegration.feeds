﻿using System;
using Convey.CQRS.Events;

namespace eBayMicroservices.Feeds.Application.Events
{
    
    public class EBayApiExceptionEvent : IEvent
    {
        public Guid OperationId { get; }
        public string Context { get; }
        public int? StatusCode { get; }
        public string Reason { get; }
        public string Code { get; }

        public EBayApiExceptionEvent(Guid operationId, string context, int? statusCode, string reason)
        {
            OperationId = operationId;
            Context = context;
            StatusCode = statusCode;
            Reason = reason;
            Code = $"eBayApiException. Ctx: {context}.OperationId: {operationId} StatusCode: {statusCode}";
        }
    }
}