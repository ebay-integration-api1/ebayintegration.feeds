﻿using eBayMicroservices.Feeds.Application.Exceptions.Abstract;

namespace eBayMicroservices.Feeds.Application.Exceptions.Concrete
{
    public class UnauthorisedUserException : AppException
    {
        public override string Code => "unauthorised_user";

        public UnauthorisedUserException() : base("Unauthorised user in secured method")
        {
            
        }

    }
}