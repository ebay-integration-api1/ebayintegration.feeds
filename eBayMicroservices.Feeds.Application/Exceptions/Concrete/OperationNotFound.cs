﻿using eBayMicroservices.Feeds.Application.Exceptions.Abstract;

namespace eBayMicroservices.Feeds.Application.Exceptions.Concrete
{
    public class OperationNotFound : AppException
    {
        public override string Code => "operation_not_found";
        public string Id { get; }

        public OperationNotFound(string id) : base($"operation with id : {id} not found")
        {
            Id = id;
        }

    }
}