﻿namespace eBayMicroservices.Feeds.Application.Exceptions.Concrete
{
    public enum EBayApiExceptionContext
    {
        RequestOrderReport = 1,
        GetOrderReportState = 2,
        GetFile = 3
    }
}