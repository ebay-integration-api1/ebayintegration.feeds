﻿using eBayMicroservices.Feeds.Application.Exceptions.Abstract;

namespace eBayMicroservices.Feeds.Application.Exceptions.Concrete
{
    public class GetOrderReportResultException : AppException
    {
        public string Identifier { get; }

        public GetOrderReportResultException(string identifier) : base($"get order report result exception. Id : {identifier}")
        {
            Identifier = identifier;
        }

        public override string Code => "get_order_report_result_exception";
    }
}