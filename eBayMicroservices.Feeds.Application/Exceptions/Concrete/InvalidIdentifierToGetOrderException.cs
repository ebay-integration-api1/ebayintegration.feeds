﻿using eBayMicroservices.Feeds.Application.Exceptions.Abstract;

namespace eBayMicroservices.Feeds.Application.Exceptions.Concrete
{
    public class InvalidIdentifierToGetOrderException : AppException
    {
        public string Identifier { get; }

        public InvalidIdentifierToGetOrderException(string identifier) : base($"invalid identifier: {identifier}")
        {
            Identifier = identifier;
        }

        public override string Code => "invalid_identifier_to_get_order";
    }
}