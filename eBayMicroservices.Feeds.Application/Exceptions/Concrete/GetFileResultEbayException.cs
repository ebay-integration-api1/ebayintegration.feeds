﻿using eBayMicroservices.Feeds.Application.Exceptions.Abstract;

namespace eBayMicroservices.Feeds.Application.Exceptions.Concrete
{
    public class GetFileResultEbayException : AppException
    {
        public int StatusCode { get; }

        public GetFileResultEbayException(int statusCode) : base($"Get file result eBay exception exited with code {statusCode}")
        {
            StatusCode = statusCode;
        }

        public override string Code => "get_file_result_eBay_exception";
    }
}