﻿using System;
using eBayMicroservices.Feeds.Application.Exceptions.Abstract;

namespace eBayMicroservices.Feeds.Application.Exceptions.Concrete
{
    public class EBayApiException : AppException
    {
        public Guid OperationId { get; }
        public EBayApiExceptionContext Context { get; }
        public int? StatusCode { get; }

        public EBayApiException(Guid operationId,int? statusCode,string content,EBayApiExceptionContext context) : base($"Content: {content}")
        {
            OperationId = operationId;
            StatusCode = statusCode;
            Context = context;
        }

        public override string Code => $"eBay Api Exception. HttpCode: {StatusCode}";
    }
}