﻿using System;

namespace eBayMicroservices.Feeds.Application.Exceptions.Abstract
{
    public abstract class AppException : Exception
    {
        public abstract string Code { get; }

        protected AppException(string message) : base(message)
        {
        }
    }
}