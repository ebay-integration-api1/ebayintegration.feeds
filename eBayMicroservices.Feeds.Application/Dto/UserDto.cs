﻿using System;

namespace eBayMicroservices.Feeds.Application.Dto
{
    public class UserDto
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Login { get; set; }
        public string AccessToken { get; set; }
    }
}