﻿using System.IO;

namespace eBayMicroservices.Feeds.Application.Dto
{
    public class OrderReportDto
    {
        public string RequestId { get; set; }
        public Stream File { get; set; }
    }
}