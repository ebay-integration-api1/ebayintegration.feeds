﻿using System;

namespace eBayMicroservices.Feeds.Application.Dto
{
    public class ReportOrderOperationDto
    {
        public Guid OperationId { get; set; }
        public Guid UserId { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public string Marketplace { get; set; }
    }
}