﻿using System;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using eBayMicroservices.Feeds.Api;
using eBayMicroservices.Feeds.Application.Dto;
using eBayMicroservices.Feeds.Tests.Shared.Factories;
using Newtonsoft.Json;
using Shouldly;
using Xunit;

namespace eBayMicroservices.Feeds.Tests.EndToEnd.Sync
{
    [Collection("E2E")]
    public class GetOrderReportResultTests : IDisposable,IClassFixture<EBayMicroserviceAppFactory<Program>>
    {
        private Task<HttpResponseMessage> Act(Guid userId,string id)
        {
            string url = @"api/reports/orders/" + $"{id}";
            _httpClient.DefaultRequestHeaders.Authorization =new AuthenticationHeaderValue(userId.ToString());
            return _httpClient.GetAsync(url);
        }

        [Fact]
        public async Task get_with_invalid_request_id_should_return_404()
        {
            Guid userId = Guid.NewGuid();
            const string emptyId = "";
            HttpResponseMessage response = await Act(userId,emptyId);
            
            response.ShouldNotBeNull();
        
            response.StatusCode.ShouldBe(HttpStatusCode.NotFound);
        }
        
        [Fact]
        public async Task get_not_existing_report_should_return_404()
        {
            Guid userId = Guid.NewGuid();
            const string emptyId = "notFound";
            HttpResponseMessage response = await Act(userId,emptyId);
            
            response.ShouldNotBeNull();
        
            response.StatusCode.ShouldBe(HttpStatusCode.NotFound);
        }
        
        [Fact]
        public async Task get_existing_report_should_be_succeed()
        {
            Guid userId = Guid.NewGuid();
            const string emptyId = "anyExistingId";
            HttpResponseMessage response = await Act(userId,emptyId);
            
            response.ShouldNotBeNull();
        
            response.StatusCode.ShouldBe(HttpStatusCode.OK);
            
            response.Content.ShouldNotBeNull();
            
        }
        
        
        #region Arrange

        private readonly HttpClient _httpClient;
        

        public GetOrderReportResultTests(EBayMicroserviceAppFactory<Program> factory)
        {
            factory.Server.AllowSynchronousIO = true;
            _httpClient = factory.CreateClient();
        }

        #endregion

        public void Dispose()
        {
            //_mongoDbFixture.Dispose();
        }
    }
}