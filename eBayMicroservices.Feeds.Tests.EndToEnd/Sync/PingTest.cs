﻿using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using eBayMicroservices.Feeds.Api;
using eBayMicroservices.Feeds.Tests.Shared.Factories;
using Shouldly;
using Xunit;

namespace eBayMicroservices.Feeds.Tests.EndToEnd.Sync
{
    [Collection("E2E")]
    public class PingTest : IDisposable,IClassFixture<EBayMicroserviceAppFactory<Program>>
    {
        private Task<HttpResponseMessage> Act() => _httpClient.GetAsync($"ping");

        [Fact]
        public async Task get_existing_user_should_return_dto()
        {
            HttpResponseMessage response = await Act();
            
            response.ShouldNotBeNull();
        
            response.StatusCode.ShouldBe(HttpStatusCode.OK);
        }
        
        #region Arrange

        private readonly HttpClient _httpClient;
        

        public PingTest(EBayMicroserviceAppFactory<Program> factory)
        {
            factory.Server.AllowSynchronousIO = true;
            _httpClient = factory.CreateClient();
            
        }

        #endregion

        public void Dispose()
        {
            //_mongoDbFixture.Dispose();
        }
    }
}