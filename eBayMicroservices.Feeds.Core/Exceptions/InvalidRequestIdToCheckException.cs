﻿using System;
using eBayMicroservices.Feeds.Core.Entities;
using eBayMicroservices.Feeds.Core.Exceptions.Abstract;

namespace eBayMicroservices.Feeds.Core.Exceptions
{
    public class InvalidRequestIdToCheckException : DomainException
    {
        public Guid OperationId { get; }
        public string GivenRequest { get; }
        public OperationType Type { get; }

        public InvalidRequestIdToCheckException(Guid operationId, string givenRequestId, OperationType type) 
            : base($"Invalid request to check. Operation: {operationId}, Sent requestId: {givenRequestId}, Type: {type}")
        {
            OperationId = operationId;
            GivenRequest = givenRequestId;
            Type = type;
        }

        public override string Code => "invalid_request_id_to_check";
    }
}