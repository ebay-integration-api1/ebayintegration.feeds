﻿using System;
using eBayMicroservices.Feeds.Core.Events;

namespace eBayMicroservices.Feeds.Core.Entities
{
    public class OrderReportOperation : OperationToExecute
    {
        public DateTime From { get; }
        public DateTime To { get; }

        public OrderReportOperation(Guid id, DateTime creationDateTime, Guid userId, string marketPlace,
            OperationType type, DateTime from, DateTime to, long? lastExecuteDateTime = null,
            Guid? operationParentId = null) 
            : base(id, creationDateTime, userId, marketPlace, type, lastExecuteDateTime, operationParentId)
        {
            From = from;
            To = to;
        }

        public static OrderReportOperation Create(Guid id, DateTime creationDateTime, Guid userId, string marketPlace,
            OperationType type, DateTime from, DateTime to, Guid? operationParentId = null)
        {
            OrderReportOperation orderReportOperation = new OrderReportOperation(id, creationDateTime, userId,
                marketPlace, type, from, to, null, operationParentId);

            OperationToExecuteAdded @event = new OperationToExecuteAdded(orderReportOperation);

            orderReportOperation.AddEvent(@event);

            return orderReportOperation;
        }

        public void MarkAsCreated(string eBayRequestId)
        {
            AddEvent(new RequestOrderReportExecuted(this, eBayRequestId));
        }

        public void MarkAsFailed(Exception ex)
        {
            AddEvent(new RequestOrderReportFailed(this,ex));
        }
    }
}