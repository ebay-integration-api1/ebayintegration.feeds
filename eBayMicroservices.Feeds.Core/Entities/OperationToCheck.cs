﻿using System;
using eBayMicroservices.Feeds.Core.Events;
using eBayMicroservices.Feeds.Core.Exceptions;

namespace eBayMicroservices.Feeds.Core.Entities
{
    public class OperationToCheck : AggregateRoot
    {
        public DateTime? LastCompletedCheck { get; }
        public DateTime? CheckingStartDateTime { get; }
        public DateTime CreationDateTime { get; }
        public string EBayRequestId { get; }
        public Guid UserId { get; }
        public string MarketPlace { get; }
        public OperationType Type { get; }

        public OperationToCheck(Guid id, string eBayRequestId, Guid userId, string marketPlace, OperationType type, DateTime? lastCompletedCheck = null, DateTime? checkingStartDateTime = null)
        {
            Id = new AggregateId(id);
            EBayRequestId = eBayRequestId;
            UserId = userId;
            MarketPlace = marketPlace;
            Type = type;
            LastCompletedCheck = lastCompletedCheck;
            CheckingStartDateTime = checkingStartDateTime;
            CreationDateTime = DateTime.UtcNow;
        }

        public static OperationToCheck Create(Guid id,string eBayRequestId,Guid userId,string marketPlace,OperationType operationType)
        {
            if (string.IsNullOrWhiteSpace(eBayRequestId))
            {
                throw new InvalidRequestIdToCheckException(id,eBayRequestId,operationType);
            }
            
            OperationToCheck operationToCheck = new OperationToCheck(id,eBayRequestId,userId,marketPlace,operationType);
            
            operationToCheck.AddEvent(new OperationToCheckAdded(operationToCheck));

            return operationToCheck;
        }

        public void MarkAsCheckingFinish()
        {
            AddEvent(new RequestOrderReportIsReady(this));
        }
        
    }
}