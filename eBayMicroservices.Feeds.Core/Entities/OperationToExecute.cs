﻿using System;
using eBayMicroservices.Feeds.Core.Events.Abstract;

namespace eBayMicroservices.Feeds.Core.Entities
{
    public abstract class OperationToExecute : AggregateRoot
    {
        public DateTime CreationDateTime { get; }
        public Guid UserId { get; }
        public string MarketPlace { get; }
        public OperationType Type { get; }
        public Guid? OperationParentId { get; }
        public long? LastExecuteDateTime { get; }

        protected OperationToExecute(Guid id,DateTime creationDateTime, Guid userId, string marketPlace, OperationType type, long? lastExecuteDateTime=null, Guid? operationParentId = null)
        {
            Id = new AggregateId(id);
            CreationDateTime = creationDateTime;
            UserId = userId;
            MarketPlace = marketPlace;
            Type = type;
            LastExecuteDateTime = lastExecuteDateTime;
            OperationParentId = operationParentId;
        }

        protected void AddDomainEvent(IDomainEvent domainEvent)
        {
            AddEvent(domainEvent);
        }
    }
}