﻿using eBayMicroservices.Feeds.Core.Entities;
using eBayMicroservices.Feeds.Core.Events.Abstract;

namespace eBayMicroservices.Feeds.Core.Events
{
    public class OperationToCheckAdded : IDomainEvent
    {
        public OperationToCheck OperationToCheck { get; }

        public OperationToCheckAdded(OperationToCheck operationToCheck)
        {
            OperationToCheck = operationToCheck;
        }
    }
}