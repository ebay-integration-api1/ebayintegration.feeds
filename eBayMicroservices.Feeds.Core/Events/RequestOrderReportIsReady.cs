﻿using eBayMicroservices.Feeds.Core.Entities;
using eBayMicroservices.Feeds.Core.Events.Abstract;

namespace eBayMicroservices.Feeds.Core.Events
{
    public class RequestOrderReportIsReady : IDomainEvent
    {
        public OperationToCheck OperationToCheck { get; }

        public RequestOrderReportIsReady(OperationToCheck operationToCheck)
        {
            OperationToCheck = operationToCheck;
        }
    }
}