﻿using eBayMicroservices.Feeds.Core.Entities;
using eBayMicroservices.Feeds.Core.Events.Abstract;

namespace eBayMicroservices.Feeds.Core.Events
{
    public class RequestOrderReportExecuted : IDomainEvent
    {
        public RequestOrderReportExecuted(OrderReportOperation operation, string eBayRequestId)
        {
            Operation = operation;
            EBayRequestId = eBayRequestId;
        }

        public OrderReportOperation Operation { get; }
        public string EBayRequestId { get; }
    }
}