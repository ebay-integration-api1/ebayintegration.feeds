﻿using eBayMicroservices.Feeds.Core.Entities;
using eBayMicroservices.Feeds.Core.Events.Abstract;

namespace eBayMicroservices.Feeds.Core.Events
{
    public class OperationToExecuteAdded : IDomainEvent
    {
        public OperationToExecuteAdded(OperationToExecute operationToExecute)
        {
            OperationToExecute = operationToExecute;
        }
        public OperationToExecute OperationToExecute { get; }
    }
}