﻿using System;
using eBayMicroservices.Feeds.Core.Entities;
using eBayMicroservices.Feeds.Core.Events.Abstract;

namespace eBayMicroservices.Feeds.Core.Events
{
    public class RequestOrderReportFailed : IDomainEvent
    {
        public OrderReportOperation OrderReportOperation { get; }
        public Exception Exception { get; }

        public RequestOrderReportFailed(OrderReportOperation orderReportOperation,Exception exception)
        {
            OrderReportOperation = orderReportOperation;
            Exception = exception;
        }
    }
}